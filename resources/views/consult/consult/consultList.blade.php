@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('userList')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet ">
                <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 col-sm-12">Rentang Tanggal Konsultasi</label>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       class="form-control m_datepicker_1_modal"
                                       readonly
                                       value="{{ $date_from }}"
                                       placeholder="Select time"
                                       name="date_from"/>
                            </div>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       class="form-control m_datepicker_1_modal"
                                       readonly
                                       value="{{ $date_to }}"
                                       placeholder="Select time"
                                       name="date_to"/>
                            </div>
                            <label class="col-form-label col-lg-1 col-sm-12 font-weight-bold">Nama Pasien</label>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       name="name"
                                       value="{{ $name }}"
                                       class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <button type="submit" class="btn btn-accent m-btn--pill btn-sm">
                            <i class="la la-search"></i> Filter Data
                        </button>
                    </div>
                </form>
            </div>

            <div class="m-portlet akses-list">
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <table class="akses-list table table-bordered datatable-new"
                               data-url="{{ route('consultDataTable') }}"
                               data-date-from="{{ $date_from }}"
                               data-date-to="{{ $date_to }}"
                               data-name="{{ $name }}"
                               data-column="{{ json_encode($datatable_column) }}">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th class="no-sort">Nama</th>
                                <th class="no-sort">Berat Badan</th>
                                <th class="no-sort">Umur</th>
                                <th class="no-sort">Lokasi</th>
                                <th class="no-sort">Waktu Konsultasi</th>
                                <th class="no-sort" width="60">Menu</th>
                            </tr>
                            </thead>
                            <tbody>
{{--                            @foreach($consult as $key => $row)--}}
{{--                            <tr>--}}
{{--                                <td>{{ $key + 1 }}.</td>--}}
{{--                                <td>{{ $row->name }}</td>--}}
{{--                                <td>{{ $row->weight }} Kg</td>--}}
{{--                                <td>{{ Main::format_age($row->birthday) }}</td>--}}
{{--                                <td>{{ ucwords($row->consult_location) }}</td>--}}
{{--                                <td>{{ Main::format_datetime($row->consult_time) }}</td>--}}
{{--                                <td>--}}
{{--                                    <div class="dropdown">--}}
{{--                                        <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill" type="button"--}}
{{--                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"--}}
{{--                                                aria-expanded="false">--}}
{{--                                            Menu--}}
{{--                                        </button>--}}
{{--                                        <div class="dropdown-menu dropdown-menu-right"--}}
{{--                                             aria-labelledby="dropdownMenuButton">--}}
{{--                                            <a class="dropdown-item btn-modal-general"--}}
{{--                                               data-route="{{ route('consultDoneModal', ['id_consult' => Main::encrypt($row->id_consult)]) }}"--}}
{{--                                               href="#">--}}
{{--                                                <i class="la la-check"></i>--}}
{{--                                                Konsultasi Selesai--}}
{{--                                            </a>--}}
{{--                                            <a class="dropdown-item btn-modal-general"--}}
{{--                                               href="#"--}}
{{--                                                data-route="{{ route('consultCancelModal', ['id_consult' => Main::encrypt($row->id_consult)]) }}">--}}
{{--                                                <i class="la la-close"></i>--}}
{{--                                                Konsultasi Batal--}}
{{--                                            </a>--}}
{{--                                            <div class="dropdown-divider"></div>--}}
{{--                                            <a class="dropdown-item btn-edit"--}}
{{--                                               href="#"--}}
{{--                                               data-route="{{ route('consultEdit', ['id_consult' => Main::encrypt($row->id_consult)]) }}">--}}
{{--                                                <i class="la la-pencil"></i>--}}
{{--                                                Edit--}}
{{--                                            </a>--}}
{{--                                            <a class="dropdown-item btn-modal-general"--}}
{{--                                               href="#"--}}
{{--                                                data-route="{{ route('consultDetail', ['id_consult' => Main::encrypt($row->id_consult)]) }}">--}}
{{--                                                <i class="la la-info"></i>--}}
{{--                                                Detail--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            @endforeach--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
