<form action="{{ route('consultCancelProcess', ['id_consult'=>Main::encrypt($row->id_consult)]) }}" method="post" class="form-send">

    {{ csrf_field() }}

    <div class="modal" id="modal-general" role="dialog"
         aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="la la-close"></i> Konsultasi Batal
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Catatan Konsultasi Batal</label>
                        <div class="col-lg-8">
                            <textarea name="consult_cancel_notes" class="form-control m-input m-input--pill"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
