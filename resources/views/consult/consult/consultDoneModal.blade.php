<form action="{{ route('consultDoneProcess', ['id_consult'=>Main::encrypt($row->id_consult)]) }}" method="post" class="form-send">

    {{ csrf_field() }}

    <div class="modal" id="modal-general" role="dialog"
         aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="la la-check"></i> Konsultasi Selesai
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Jadwal Kalender</label>
                        <div class="col-lg-8">
                            {{--                            <button type="button" class="btn btn-primary m-btn--pill" data-toggle="modal"--}}
                            {{--                                    data-target="#modal-appointment-schedule">--}}
                            {{--                                <i class="la la-eye"></i> Lihat Jadwal--}}
                            {{--                            </button>--}}
                            <a href="{{ route('scheduleCalendarPage') }}" class="btn btn-primary m-btn--pill" target="_blank">
                                <i class="la la-eye"></i> Lihat Jadwal
                            </a>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tanggal Finalize</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                   name="action_date" autocomplete="off" type="text" value="{{ Main::format_date($row->consult_time) }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Jam Finalize</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_timepicker_1_modal"
                                   name="action_hours" autocomplete="off" type="text" value="{{ date('H:i:s') }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Reminder Pasien Untuk Finalize</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="whatsapp_send"
                                           value="yes" checked> Ya
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="whatsapp_send"
                                           value="no"> Tidak
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Catatan Konsultasi Selesai</label>
                        <div class="col-lg-8">
                            <textarea name="consult_done_notes" class="form-control m-input m-input--pill"></textarea>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <div class="dropdown-divider"></div>
                    <br/>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">akan diReminder via</label>
                        <div class="col-lg-8">
                            <div class="pull-left border-line">
                                <img src="{{ asset('images/whatsapp-logo.png') }}" width="50">
                                <br/>
                                WhatsApp untuk Pasien
                            </div>
                            <div class="pull-left border-line">
                                <img src="{{ asset('images/email-logo.png') }}" width="50">
                                <br/>
                                Email untuk Admin
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
