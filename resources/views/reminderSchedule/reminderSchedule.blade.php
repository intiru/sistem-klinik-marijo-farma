<h1 align="center">Jadwal Pendaftaran Pasien</h1>
<h3 align="center">Tanggal : {{ $date_filter }}</h3>

<?php
$style = 'style="border-style:solid; border-width:1px; border-color:#000000;"';
$style_2 = 'style="border-style:solid; border-width:1px; border-color:#000000;padding: 4px 6px"';
?>

<table width="100%" <?php echo $style ?> cellpadding="2" cellspacing="0">
    <thead>
    <tr>
        <th width="20" <?php echo $style ?>>No</th>
        <th <?php echo $style ?>>Keperluan</th>
        <th <?php echo $style ?>>Metode Tindakan</th>
        <th <?php echo $style ?>>Nama</th>
        <th <?php echo $style ?>>Berat Badan</th>
        <th <?php echo $style ?>>Umur</th>
        <th <?php echo $style ?>>Waktu Pendaftaran</th>
        <th <?php echo $style ?>>Lokasi</th>
    </tr>
    </thead>
    <tbody>
    @foreach($appointment as $key=>$row)
        @php
            $location = '';
                if($row->need_type == 'consult') {
                    $location = \app\Models\mConsult::select('consult_location')->where('id_consult', $row->id_consult)->value('consult_location');
                } else if($row->need_type == 'action') {
                    $location = \app\Models\mAction::select('action_location')->where('id_action', $row->id_action)->value('action_location');
                } else if($row->need_type == 'control') {
                    $location = \app\Models\mControl::select('control_location')->where('id_control', $row->id_control)->value('control_location');
                }
        @endphp


        <tr>
            <td <?php echo $style ?>>{{ $key + 1 }}.</td>
            <td <?php echo $style ?>>{!! Main::need_type_style($row->need_type, $row->control_step) !!}</td>
            <td <?php echo $style ?>>{{ $row->action_method_title }}</td>
            <td <?php echo $style ?>>{{ $row->name }}</td>
            <td <?php echo $style ?>>{{ $row->weight }} Kg</td>
            <td <?php echo $style ?>>{{ Main::format_age($row->birthday) }}</td>
            <td <?php echo $style ?>>{{ Main::format_datetime($row->appointment_time) }}</td>
            <td <?php echo $style ?>>{{ ucwords($location) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
