<div class="modal" id="modal-general" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">

            <ul class="nav nav-tabs" role="tablist" style="background-color: #e8e8e8">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-patient">
                        1. Detail Pasien
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-appointment">
                        2. Pendaftaran
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-consult">
                        3. Konsultasi
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-finalize">
                        4. Finalize
                    </a>
                </li>
                @foreach($row->control->control_list as $key=>$row_data)
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab-follow-up-{{ $key }}">
                            {{ 5 + $key }}. Follow Up #{{ $row_data->control_number }}
                        </a>
                    </li>
                @endforeach
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-done">
                        {{ $key + 6 }}. Selesai
                    </a>
                </li>
            </ul>
            <div class="modal-body">

                <div class="tab-content">
                    <div class="tab-pane active" id="tab-patient" role="tabpanel">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Nama</label>
                            <div class="col-lg-8">
                                {{ $row->patient->name }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Umur</label>
                            <div class="col-lg-8">
                                {{ Main::format_age($row->patient->birthday) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Tanggal Lahir</label>
                            <div class="col-lg-8">
                                {{ Main::format_date_label($row->patient->birthday) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Provinsi</label>
                            <div class="col-lg-8">
                                {{ $row->patient->province->province_name }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Kabupaten</label>
                            <div class="col-lg-8">
                                {{ $row->patient->city->city_name }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Kecamatan</label>
                            <div class="col-lg-8">
                                {{ $row->patient->subdistrict->subdistrict_name }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Alamat</label>
                            <div class="col-lg-8">
                                {{ $row->patient->address }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Berat Badan</label>
                            <div class="col-lg-8">
                                {{ $row->patient->weight }} KG
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">No HP/WhatsApp</label>
                            <div class="col-lg-8">
                                {{ $row->patient->phone_1 }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">No HP Lainnya</label>
                            <div class="col-lg-8">
                                {{ $row->patient->phone_2 }}
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-appointment" role="tabpanel">

                        @if($row->control->action->consult)
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">Waktu Pendaftaran</label>
                                <div class="col-lg-8">
                                    {{ Main::format_datetime($row->control->action->consult->appointment->appointment_time) }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">Keperluan saat Pendaftaran</label>
                                <div class="col-lg-8">
                                    {{ Main::need_type_label($row->control->action->consult->appointment->need_type) }}
                                </div>
                            </div>
                        @else
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">Waktu Pendaftaran</label>
                                <div class="col-lg-8">
                                    {{ Main::format_datetime($row->control->action->appointment->appointment_time) }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">Keperluan saat Pendaftaran</label>
                                <div class="col-lg-8">
                                    {{ Main::need_type_label($row->control->action->appointment->need_type) }}
                                </div>
                            </div>
                        @endif

                    </div>

                    <div class="tab-pane" id="tab-consult" role="tabpanel">

                        @if($row->control->action->consult)
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">Waktu Konsultasi</label>
                                <div class="col-lg-8">
                                    {{ Main::format_datetime($row->control->action->consult->consult_time) }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">Tempat Konsultasi</label>
                                <div class="col-lg-8">
                                    {{ ucwords($row->control->action->consult->consult_location) }}
                                </div>
                            </div>
                        @else
                            <div class="form-group m-form__group row">
                                <label class="col-lg-12 col-form-label">
                                    Dari Pendaftaran Langsung ke Finalize
                                </label>
                            </div>
                        @endif
                    </div>

                    <div class="tab-pane" id="tab-finalize" role="tabpanel">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Waktu Finalize</label>
                            <div class="col-lg-8">
                                {{ Main::format_datetime($row->control->action->action_time) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Lokasi Finalize</label>
                            <div class="col-lg-8">
                                {{ ucwords($row->control->action->action_location) }}
                            </div>
                        </div>
                    </div>

                    @foreach($row->control->control_list as $key=>$row_data)
                        <div class="tab-pane" id="tab-follow-up-{{ $key }}" role="tabpanel">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">Waktu Follow Up</label>
                                <div class="col-lg-8">
                                    {{ Main::format_datetime($row_data->control_time) }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">Lokasi Follow Up</label>
                                <div class="col-lg-8">
                                    {{ ucwords($row_data->control_location) }}
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="tab-pane" id="tab-done" role="tabpanel">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Waktu Selesai</label>
                            <div class="col-lg-8">
                                {{ Main::format_datetime($row->done_time) }}
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
