<form action="{{ route('controlDoneProcess', ['id_control'=>\app\Helpers\Main::encrypt($control->id_control)]) }}"
      method="post"
      class="form-send"
      data-alert-show="true"
      data-alert-field-message="true">

    {{ csrf_field() }}

    <div class="modal" id="modal-general" role="dialog"
         aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="la la-check"></i> Kontrol Selesai
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Perlu Kontrol Lanjutan ?</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="need_control" value="yes"> Ya
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="need_control" value="no" checked> Tidak
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="control-wrapper-next hidden">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Jadwal Kalender</label>
                            <div class="col-lg-8">
                                {{--                            <button type="button" class="btn btn-primary m-btn--pill" data-toggle="modal"--}}
                                {{--                                    data-target="#modal-appointment-schedule">--}}
                                {{--                                <i class="la la-eye"></i> Lihat Jadwal--}}
                                {{--                            </button>--}}
                                <a href="{{ route('scheduleCalendarPage') }}" class="btn btn-primary m-btn--pill" target="_blank">
                                    <i class="la la-eye"></i> Lihat Jadwal
                                </a>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Waktu Kontrol Sebelumnya</label>
                            <div class="col-lg-8" style="padding-top: 10px">
                                {{ Main::format_datetime($control->control_time) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Tanggal Kontrol</label>
                            <div class="col-lg-8">
                                <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                       name="control_date" autocomplete="off" type="text" value="{{ Main::format_date($control_date_next) }}">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Jam Kontrol</label>
                            <div class="col-lg-8">
                                <input class="form-control m-input m-input--pill m_timepicker_1_modal"
                                       name="control_hours" autocomplete="off" type="text" value="{{ date('H:i:s') }}">
                            </div>
                        </div>
                    </div>
                    <div class="control-wrapper-done">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Catatan Kontrol</label>
                            <div class="col-lg-8">
                                <textarea class="form-control m-input m-input--pill" name="control_done_notes"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="control-wrapper-next hidden">
                        <br/>
                        <div class="dropdown-divider"></div>
                        <br/>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">akan diReminder via</label>
                            <div class="col-lg-8">
                                <div class="pull-left border-line">
                                    <img src="{{ asset('images/whatsapp-logo.png') }}" width="50">
                                    <br/>
                                    WhatsApp untuk Pasien
                                </div>
                                <div class="pull-left border-line">
                                    <img src="{{ asset('images/email-logo.png') }}" width="50">
                                    <br/>
                                    Email untuk Admin
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</form>
