@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <form action="{{ route('reminderSettingUpdate') }}" method="post"
              class="form-send m-form m-form--fit m-form--label-align-right m-form--group-seperator">

            {{ csrf_field() }}

            <div class="m-content">

                <div class="m-portlet akses-list">
                    <div class="m-portlet__body akses-list">

                        @foreach($reminder_setting as $row)

                            <div class="form-group m-form__group row">
                                <label class="col-lg-2 col-form-label">{{ $row->label }}</label>
                                <div class="col-lg-1">
                                    <div class="m-checkbox-list" style="margin-top: 10px">
                                        <label class="m-checkbox m-checkbox--success">
                                            <input type="checkbox" name="status[{{ $row->id_reminder_setting }}]"
                                                   value="active" {{ $row->status == 'active' ? 'checked':'' }}>
                                            Aktifkan
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <input class="form-control m-input m-input--pill" id="m_touchspin_4"
                                           name="target_day[{{ $row->id_reminder_setting }}]"
                                           value="{{ $row->target_day }}"
                                           type="text">
                                    <span class="m-form__help"> {!! $row->description  !!} </span>
                                </div>
                                <label class="col-lg-2 col-form-label">Jam Reminder</label>
                                <div class="col-lg-2">
                                    <select name="target_hour[{{ $row->id_reminder_setting }}]"
                                            class="form-control m-select2">
                                        @for($i=0; $i<=24; $i++)
                                            @php($jam = $i <= 9 ? '0'.$i : $i)
                                            <option value="{{  $jam.':00:00' }}" {{ $row->target_hour == $jam.':00:00' ? 'selected':'' }}>{{ $jam.':00:00' }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    @if($row->via == 'whatsapp')
                                        <img src="{{ asset('images/whatsapp-logo.png') }}" width="40">
                                        via WhatsApp
                                    @else

                                        <img src="{{ asset('images/email-logo.png') }}" width="40">
                                        via Email

                                    @endif

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="produksi-buttons akses-update">
                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <i class="la la-check"></i> Simpan
                    </button>
                </div>

            </div>
        </form>

    </div>
@endsection
