@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

{{--    @php--}}
{{--        $download_pdf = \Illuminate\Support\Facades\Session::get('download_pdf');--}}
{{--        $id_patient = \Illuminate\Support\Facades\Session::get('id_patient');--}}
{{--        $id_payment = \Illuminate\Support\Facades\Session::get('id_payment');--}}
{{--    @endphp--}}

{{--    @if($download_pdf == 'yes')--}}
{{--        <script type="text/javascript">--}}
{{--            window.open("{{ route('patientMedicRecordPrint', ['id_patient' =>  \app\Helpers\Main::encrypt($id_patient)]) }}");--}}
{{--            window.open("{{ route('paymentPrint', ['id_action' => \app\Helpers\Main::encrypt($id_payment) ]) }}")--}}
{{--        </script>--}}

{{--        @php--}}
{{--            \Illuminate\Support\Facades\Session::put([--}}
{{--                'download_pdf' => 'no'--}}
{{--            ]);--}}
{{--        @endphp--}}

{{--    @endif--}}


    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet ">
                <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 col-sm-12">Rentang Tanggal Tindakan</label>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       class="form-control m_datepicker_1_modal"
                                       readonly
                                       value="{{ $date_from }}"
                                       placeholder="Select time"
                                       name="date_from"/>
                            </div>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       class="form-control m_datepicker_1_modal"
                                       readonly
                                       value="{{ $date_to }}"
                                       placeholder="Select time"
                                       name="date_to"/>
                            </div>
                            <label class="col-form-label col-lg-1 col-sm-12 font-weight-bold">Nama Pasien</label>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       name="name"
                                       value="{{ $name }}"
                                       class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <button type="submit" class="btn btn-accent m-btn--pill btn-sm">
                            <i class="la la-search"></i> Filter Data
                        </button>
                    </div>
                </form>
            </div>


            <ul class="nav nav-tabs" style="margin-bottom: 0">
                <li class="nav-item">
                    <a href="{{ route('actionProcessList') }}" class="nav-link active">
                        <h5>1. Daftar Menunggu Tindakan</h5>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('actionDoneList') }}">
                        <h5>2. Daftar Tindakan Selesai</h5>
                    </a>
                </li>
            </ul>

            <div class="m-portlet akses-list">
                <div class="m-portlet__body">
                    <div class="table-responsive">

                        <table class="akses-action_wait_list table table-bordered datatable-new"
                               data-url="{{ route('actionProcessDataTable') }}"
                               data-date-from="{{ $date_from }}"
                               data-date-to="{{ $date_to }}"
                               data-name="{{ $name }}"
                               data-column="{{ json_encode($datatable_column) }}">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th class="no-sort">Nama</th>
                                <th class="no-sort">Berat Badan</th>
                                <th class="no-sort">Umur</th>
{{--                                <th class="no-sort">Lokasi</th>--}}
                                <th class="no-sort">Waktu {{ Main::need_type_label('action') }}</th>
                                <th class="no-sort" width="60">Menu</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
