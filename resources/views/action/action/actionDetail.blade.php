<div class="modal" id="modal-general" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">

            <ul class="nav nav-tabs" role="tablist" style="background-color: #e8e8e8">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-patient">
                        1. Detail Pasien
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-appointment">
                        2. Pendaftaran
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-consult">
                        3. Konsultasi
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-finalize">
                        4. Tindakan
                    </a>
                </li>
            </ul>
            <div class="modal-body">

                <div class="tab-content">
                    <div class="tab-pane active" id="tab-patient" role="tabpanel">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Nama Pasien</label>
                            <div class="col-lg-8">
                                {{ $row->patient->name }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Nama Penanggung Jawab</label>
                            <div class="col-lg-8">
                                {{ $row->patient->name_responsible }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Hubungan Pasien dengan Penanggung Jawab</label>
                            <div class="col-lg-8">
                                {{ $row->patient->name_responsible_relation }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Umur</label>
                            <div class="col-lg-8">
                                {{ Main::format_age($row->patient->birthday) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Tempat Lahir</label>
                            <div class="col-lg-8">
                                {{ $row->patient->birth_place }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Tanggal Lahir</label>
                            <div class="col-lg-8">
                                {{ Main::format_date_label($row->patient->birthday) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Berat Badan</label>
                            <div class="col-lg-8">
                                {{ $row->patient->weight }} KG
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Suhu Tubuh</label>
                            <div class="col-lg-8">
                                {{ $row->patient->temperature }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Keluhan Pasien</label>
                            <div class="col-lg-8">
                                {{ $row->patient->complaint }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Nomer Rekam Medis</label>
                            <div class="col-lg-8">
                                {{ $row->patient->medic_record_number }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Poli</label>
                            <div class="col-lg-8">
                                {{ $row->patient->poli }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Distrik</label>
                            <div class="col-lg-8">
                                {{ $row->patient->district }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Sub Distrik</label>
                            <div class="col-lg-8">
                                {{ $row->patient->subdistrict }}
                            </div>
                        </div>
{{--                        <div class="form-group m-form__group row">--}}
{{--                            <label class="col-lg-4 col-form-label">Provinsi</label>--}}
{{--                            <div class="col-lg-8">--}}
{{--                                {{ $row->patient->province->province_name }}--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="form-group m-form__group row">--}}
{{--                            <label class="col-lg-4 col-form-label">Kabupaten</label>--}}
{{--                            <div class="col-lg-8">--}}
{{--                                {{ $row->patient->city->city_name }}--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="form-group m-form__group row">--}}
{{--                            <label class="col-lg-4 col-form-label">Kecamatan</label>--}}
{{--                            <div class="col-lg-8">--}}
{{--                                {{ $row->patient->subdistrict->subdistrict_name }}--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Alamat</label>
                            <div class="col-lg-8">
                                {{ $row->patient->address }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">No HP/WhatsApp</label>
                            <div class="col-lg-8">
                                {{ $row->patient->phone_1 }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">No HP Lainnya</label>
                            <div class="col-lg-8">
                                {{ $row->patient->phone_2 }}
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-appointment" role="tabpanel">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Waktu Pendaftaran</label>
                            <div class="col-lg-8">
                                {{ Main::format_datetime($row->appointment->appointment_time) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Keperluan saat Pendaftaran</label>
                            <div class="col-lg-8">
                                {{ Main::need_type_label($row->appointment->need_type) }}
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-consult" role="tabpanel">

                        @if($row->consult)
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Waktu Konsultasi</label>
                            <div class="col-lg-8">
                                {{ Main::format_datetime($row->consult->consult_time) }}
                            </div>
                        </div>
{{--                        <div class="form-group m-form__group row">--}}
{{--                            <label class="col-lg-4 col-form-label">Tempat Konsultasi</label>--}}
{{--                            <div class="col-lg-8">--}}
{{--                                {{ ucwords($row->consult->consult_location) }}--}}
{{--                            </div>--}}
{{--                        </div>--}}
                            @else
                            <div class="form-group m-form__group row">
                                <label class="col-lg-12 col-form-label">
                                    Dari Pendaftaran Langsung ke Tindakan
                                </label>
                            </div>
                            @endif
                    </div>

                    <div class="tab-pane" id="tab-finalize" role="tabpanel">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Waktu Finalize</label>
                            <div class="col-lg-8">
                                {{ Main::format_datetime($row->action_time) }}
                            </div>
                        </div>
{{--                        <div class="form-group m-form__group row">--}}
{{--                            <label class="col-lg-4 col-form-label">Lokasi Finalize</label>--}}
{{--                            <div class="col-lg-8">--}}
{{--                                {{ ucwords($row->action_location) }}--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
