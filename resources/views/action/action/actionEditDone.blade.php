<form action="{{ route('actionDoneUpdate', ['id_action'=> Main::encrypt($edit->id_action)]) }}" method="post"
      class="form-send">

    {{ csrf_field() }}


    <div class="modal" id="modal-general" role="dialog"
         aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="la la-pencil"></i> Edit Tindakan
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Lokasi Klinik Tujuan</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="action_location"
                                           value="panjer" {{ $edit->action_location == 'panjer' ? 'checked':'' }}>
                                    Panjer
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="action_location"
                                           value="jimbaran" {{ $edit->action_location == 'jimbaran' ? 'checked':'' }}>
                                    Jimbaran
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="action_location"
                                           value="home_service" {{ $edit->action_location == 'home_service' ? 'checked':'' }}>
                                    Home Service
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Metode Tindakan</label>
                        <div class="col-lg-8">
                            <select class="form-control m-select2" name="id_action_method">
                                @foreach($action_method as $row)
                                    <option value="{{ $row->id_action_method }}" {{ $row->id_action_method == $edit->id_action_method ? 'selected':'' }}>{{ $row->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Perbarui Data</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</form>