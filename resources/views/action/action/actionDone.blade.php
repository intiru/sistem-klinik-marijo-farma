@extends('../general/index')

@section('css')
{{--    {!! $schedule_calendar_modal['css'] !!}--}}
@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>

{{--    {!! $schedule_calendar_modal['js'] !!}--}}
@endsection

@section('body')

{{--    {!! $schedule_calendar_modal['view'] !!}--}}

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('userList')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-action">
                        <h5>1. Data Tindakan Selesai</h5>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-control">
                        <h5>2. Jadwal Kontrol</h5>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-medic-record">
                        <h5>3. Rekam Medis</h5>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-payment">
                        <h5>4. Pembayaran</h5>
                    </a>
                </li>
            </ul>

            <form action="{{ route('actionDoneProcess', ['id_action' => Main::encrypt($action->id_action)]) }}"
                  method="post"
                  class="form-send"
                  data-alert-show="true"
                  data-alert-field-message="true"
                  data-redirect="{{ route('actionProcessList') }}">

                {{ csrf_field() }}

                <input type="hidden" name="total" value="">
                <input type="hidden" name="grand_total" value="">

                <div class="tab-content">
                    <div class="tab-pane active" id="tab-action" role="tabpanel">
                        <div class="m-portlet akses-list">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                Data Tindakan Selesai
                                                <small>
                                                    Mengisi Data untuk Tindakan Selesai
                                                </small>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row m--hide">
                                    <label class="col-lg-2 col-form-label">Metode Tindakan</label>
                                    <div class="col-lg-10">
                                        <select class="form-control m-select2" name="id_action_method">
                                            @foreach($action_method as $row)
                                                <option value="{{ $row->id_action_method }}">{{ $row->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Pilihan Rawat</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="rawat">
                                            <option value="inap">Rawat Inap</option>
                                            <option value="jalan">Rawat Jalan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Catatan Tindakan Selesai</label>
                                    <div class="col-lg-10">
                                    <textarea class="form-control m-input m-input--pill"
                                              name="action_done_notes"></textarea>
                                    </div>
                                </div>
                                <br/>
                                <br/>
                                <div class="dropdown-divider"></div>
                                <br/>
                                <div class="form-group m-form__group row m--hide">
                                    <label class="col-lg-2 col-form-label">akan diReminder via</label>
                                    <div class="col-lg-10">
                                        <div class="pull-left border-line">
                                            <img src="{{ asset('images/whatsapp-logo.png') }}" width="50">
                                            <br/>
                                            WhatsApp untuk Pasien
                                        </div>
                                        <div class="pull-left border-line">
                                            <img src="{{ asset('images/email-logo.png') }}" width="50">
                                            <br/>
                                            Email untuk Admin
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-control" role="tabpanel">
                        <div class="m-portlet akses-list">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                Jadwal Kontrol
                                                <small>
                                                    Pilih jadwal kontrol untuk pasien datang selanjutnya
                                                </small>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">

                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Jadwal Kalender</label>
                                    <div class="col-lg-10">
                                        {{--                            <button type="button" class="btn btn-primary m-btn--pill" data-toggle="modal"--}}
                                        {{--                                    data-target="#modal-appointment-schedule">--}}
                                        {{--                                <i class="la la-eye"></i> Lihat Jadwal--}}
                                        {{--                            </button>--}}
                                        <a href="{{ route('scheduleDoctorPage') }}" class="btn btn-primary m-btn--pill" target="_blank">
                                            <i class="la la-eye"></i> Lihat Jadwal
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Waktu Finalize</label>
                                    <div class="col-lg-10" style="padding-top: 10px">{{ Main::format_datetime_2($action->action_time) }}</div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Lanjut Kontrol ?</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="lanjut_kontrol">
                                            <option value="yes">Ya</option>
                                            <option value="no">Tidak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Tanggal Kontrol</label>
                                    <div class="col-lg-10">
                                        <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                               name="control_date" autocomplete="off" type="text" value="{{ Main::format_date($control_date) }}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Jam Kontrol</label>
                                    <div class="col-lg-10">
                                        <input class="form-control m-input m-input--pill m_timepicker_1_modal"
                                               name="control_hours" autocomplete="off" type="text" value="{{ date('H:i:s') }}">
                                    </div>
                                </div>
                                <br/>
                                <br/>
                                <div class="dropdown-divider"></div>
                                <br/>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">akan diReminder via</label>
                                    <div class="col-lg-10">
                                        <div class="pull-left border-line">
                                            <img src="{{ asset('images/whatsapp-logo.png') }}" width="50">
                                            <br/>
                                            WhatsApp untuk Pasien
                                        </div>
                                        <div class="pull-left border-line">
                                            <img src="{{ asset('images/email-logo.png') }}" width="50">
                                            <br/>
                                            Email untuk Admin
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-medic-record" role="tabpanel">
                        <div class="m-portlet  akses-list">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                Rekam Medis
                                                <small>
                                                    Form untuk input rekam medis pasien
                                                </small>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form m-form--label-align-right">
                                <div class="m-portlet__body">
                                    <div class="m-form__section m-form__section--first">
                                        <div class="m-form__heading">
                                            <h3 class="m-form__heading-title">
                                                No Rekam Medis : {{ $action->patient['medic_record_number'] }}
                                            </h3>
                                        </div>
                                        <div class="m-form__heading">
                                            <h3 class="m-form__heading-title">
                                                Identitas Pasien :
                                            </h3>
                                        </div>
                                        <div class="form-group m-form__group row detail-info-row">
                                            <label class="col-lg-2 col-form-label">
                                                Nama
                                            </label>
                                            <div class="col-lg-10">
                                                {{ $action->patient->name }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row detail-info-row">
                                            <label class="col-lg-2 col-form-label">
                                                Umur
                                            </label>
                                            <div class="col-lg-10">
                                                {{ Main::format_age($action->patient->birthday) }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row detail-info-row">
                                            <label class="col-lg-2 col-form-label">
                                                Berat Badan
                                            </label>
                                            <div class="col-lg-10">
                                                {{ $action->patient->weight }} Kg
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row detail-info-row">
                                            <label class="col-lg-2 col-form-label">
                                                Tanggal Lahir
                                            </label>
                                            <div class="col-lg-10">
                                                {{ Main::format_date_label($action->patient->birthday) }}
                                            </div>
                                        </div>
{{--                                        <div class="m-form__group form-group row detail-info-row">--}}
{{--                                            <label class="col-lg-2 col-form-label">--}}
{{--                                                Provinsi--}}
{{--                                            </label>--}}
{{--                                            <div class="col-lg-10">--}}
{{--                                                {{ $action['patient']['province']['province_name'] }}--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="m-form__group form-group row detail-info-row">--}}
{{--                                            <label class="col-lg-2 col-form-label">--}}
{{--                                                Kabupaten--}}
{{--                                            </label>--}}
{{--                                            <div class="col-lg-10">--}}
{{--                                                {{ $action['patient']['city']['city_name'] }}--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="m-form__group form-group row detail-info-row">--}}
{{--                                            <label class="col-lg-2 col-form-label">--}}
{{--                                                Kecamatan--}}
{{--                                            </label>--}}
{{--                                            <div class="col-lg-10">--}}
{{--                                                {{ $action['patient']['subdistrict']['subdistrict_name'] }}--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="m-form__group form-group row detail-info-row">
                                            <label class="col-lg-2 col-form-label">
                                                Alamat
                                            </label>
                                            <div class="col-lg-10">
                                                {{ $action->patient->address }}
                                            </div>
                                        </div>
                                        <div class="m-form__group form-group row detail-info-row">
                                            <label class="col-lg-2 col-form-label">
                                                No HP/WhatsApp
                                            </label>
                                            <div class="col-lg-10">
                                                {{ $action->patient->phone_1 }}
                                            </div>
                                        </div>
                                        <div class="m-form__group form-group row detail-info-row">
                                            <label class="col-lg-2 col-form-label">
                                                No HP Lainnya
                                            </label>
                                            <div class="col-lg-10">
                                                {{ $action->patient->phone_2 }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__seperator m-form__seperator--dashed"></div>
                                    <div class="m-form__section m-form__section">
                                        <div class="m-form__heading">
                                            <h3 class="m-form__heading-title">
                                                Anamnesa :
                                            </h3>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label required">
                                                Diagnosa
                                            </label>
                                            <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="diagnosa">{{ isset($action->patient->medic_record->diagnosa) ? $action->patient->medic_record->diagnosa : '' }}</textarea>
                                            </div>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label required">
                                                Riwayat Penyakit Sekarang
                                            </label>
                                            <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="disease_now">{{ isset($action->patient->medic_record->disease_now) ? $action->patient->medic_record->disease_now : '' }}</textarea>
                                            </div>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label required">
                                                Riwayat Penyakit Sebelumnya
                                            </label>
                                            <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="disease_before">{{ isset($action->patient->medic_record->disease_before) ? $action->patient->medic_record->disease_before : '' }}</textarea>
                                            </div>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label required">
                                                Riwayat Pengobatan
                                            </label>
                                            <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="medical_history">{{ isset($action->patient->medic_record->medical_history) ? $action->patient->medic_record->medical_history : '' }}</textarea>
                                            </div>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label required">
                                                Riwayat Alergi
                                            </label>
                                            <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="alergy_history">{{ isset($action->patient->medic_record->alergy_history) ? $action->patient->medic_record->alergy_history : '' }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__seperator m-form__seperator--dashed"></div>
                                    <div class="m-form__section m-form__section">
                                        <div class="m-form__heading">
                                            <h3 class="m-form__heading-title">
                                                Pemeriksaan Fisik :
                                            </h3>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label required">
                                                Vital Sign - Tensi
                                            </label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control m-input m-input--pill"
                                                       name="sign_tension" value="{{ isset($action->patient->medic_record->sign_tension) ? $action->patient->medic_record->sign_tension : '' }}">
                                            </div>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label required">
                                                Vital Sign - Temperatur
                                            </label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control m-input m-input--pill"
                                                       name="sign_temp" value="{{ isset($action->patient->medic_record->sign_temp) ? $action->patient->medic_record->sign_temp : '' }}">
                                            </div>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label required">
                                                Vital Sign - Nadi
                                            </label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control m-input m-input--pill"
                                                       name="sign_pulse" value="{{ isset($action->patient->medic_record->sign_pulse) ? $action->patient->medic_record->sign_pulse : '' }}">
                                            </div>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label required">
                                                Vital Sign - RR
                                            </label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control m-input m-input--pill"
                                                       name="sign_rr" value="{{ isset($action->patient->medic_record->sign_rr) ? $action->patient->medic_record->sign_rr : '' }}">
                                            </div>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label required">
                                                Keadaan Umum
                                            </label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control m-input m-input--pill"
                                                       name="general_condition" value="{{ isset($action->patient->medic_record->general_condition) ? $action->patient->medic_record->general_condition : '' }}">
                                            </div>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label required">
                                                Pemeriksaan Genital
                                            </label>
                                            <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="genital_check">{{ isset($action->patient->medic_record->genital_check) ? $action->patient->medic_record->genital_check : '' }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__seperator m-form__seperator--dashed"></div>
                                    <div class="m-form__section m-form__section m--hide">
                                        <div class="m-form__heading">
                                            <h3 class="m-form__heading-title required">
                                                Assessment :
                                            </h3>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label">

                                            </label>
                                            <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="assessment">{{ isset($action->patient->medic_record->assessment) ? $action->patient->medic_record->assessment : '' }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__seperator m-form__seperator--dashed"></div>
                                    <div class="m-form__section m-form__section--last m--hide">
                                        <div class="m-form__heading">
                                            <h3 class="m-form__heading-title required">
                                                Planning :
                                            </h3>
                                        </div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-lg-2 col-form-label">

                                            </label>
                                            <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="planning">{{ isset($action->patient->medic_record->planning) ? $action->patient->medic_record->planning : '' }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-payment" role="tabpanel">
                        <div class="m-portlet akses-list">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                Payment
                                                <small>
                                                    Form untuk item yang perlu dibayar
                                                </small>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">

                                    <button type="button"
                                            class="btn btn-success m-btn m-btn--pill m-btn--air btn-payment-add">
                                        <i class="la la-plus"></i> Tambah Item Pembayaran
                                    </button>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <div class="table-responsive">
                                    <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general table-payment-list">
                                        <thead>
                                        <tr>
                                            <th>Deskripsi</th>
                                            <th width="100">Qty</th>
                                            <th width="150">Harga</th>
                                            <th width="150">Total</th>
                                            <th width="60">Menu</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class=" row">
                            <div class="col-xs-12 col-lg-5 offset-lg-7">
                                <div class="m-portlet m-portlet--mobile">
                                    <div class="m-portlet__body row">
                                        <div class="col-lg-12">
                                            <div class="form-group m-form__group row">
                                                <label for="example-text-input"
                                                       class="col-lg-4 col-form-label">Total</label>
                                                <div class="col-lg-8 col-form-label">
                                                    <strong><span class="payment-total">0</span></strong>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="example-text-input" class="col-lg-4 col-form-label">Biaya
                                                    Tambahan</label>
                                                <div class="col-lg-8">
                                                    <input class="form-control m-input input-numeral payment-plus"
                                                           type="text"
                                                           name="payment_plus" value="0">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="example-text-input"
                                                       class="col-lg-4 col-form-label">Potongan</label>
                                                <div class="col-lg-8">
                                                    <input class="form-control m-input input-numeral payment-cut"
                                                           type="text"
                                                           name="payment_cut" value="0">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="example-text-input" class="col-lg-4 col-form-label">Grand
                                                    Total</label>
                                                <div class="col-lg-8 col-form-label">
                                                    <strong><span class="payment-grand-total"
                                                                  style="font-size: 22px">0</span></strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="produksi-buttons ">
                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <i class="la la-check"></i> Simpan
                    </button>

                    <a href="{{ route('actionProcessList') }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <i class="la la-angle-double-left"></i>
                        Kembali ke Daftar
                    </a>
                </div>
            </form>

        </div>

    </div>

    <table class="table-payment-row hidden">
        <tbody>
        <tr>
            <td>
                <textarea class="form-control m-input m-input--pill" name="description[]"
                          style="min-width: 200px; max-width: 100%"></textarea>
            </td>
            <td class="td-payment-qty"><input type="text"
                                              class="form-control m-input m-input--pill input-numeral payment-qty"
                                              name="qty[]" value="1" style="min-width: 80px"></td>
            <td align="right" class="td-payment-price">
                <input type="text" class="form-control m-input m-input--pill input-numeral payment-price" name="price[]"
                       value="0" style="min-width: 140px">
            </td>
            <td align="right" class="payment-td-sub-total">0</td>
            <td>
                <button class="btn btn-sm btn-danger m-btn--pill btn-payment-row-remove" type="button">
                    <i class="la la-remove"></i> Hapus
                </button>
            </td>
        </tr>
        </tbody>
    </table>

@endsection
