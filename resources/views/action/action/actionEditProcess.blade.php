<form action="{{ route('actionProcessUpdate', ['id_action'=> Main::encrypt($edit->id_action)]) }}" method="post"
      class="form-send">

    {{ csrf_field() }}


    <div class="modal" id="modal-general" role="dialog"
         aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="la la-pencil"></i> Edit Tindakan
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Lokasi Klinik Tujuan</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="action_location"
                                           value="panjer" {{ $edit->action_location == 'panjer' ? 'checked':'' }}>
                                    Panjer
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="action_location"
                                           value="jimbaran" {{ $edit->action_location == 'jimbarang' ? 'checked':'' }}>
                                    Jimbaran
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="action_location"
                                           value="home_service" {{ $edit->action_location == 'home_service' ? 'checked':'' }}>
                                    Home Service
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Jadwal Kalender</label>
                        <div class="col-lg-8">
                            {{--                            <button type="button" class="btn btn-primary m-btn--pill" data-toggle="modal"--}}
                            {{--                                    data-target="#modal-appointment-schedule">--}}
                            {{--                                <i class="la la-eye"></i> Lihat Jadwal--}}
                            {{--                            </button>--}}
                            <a href="{{ route('scheduleCalendarPage') }}" class="btn btn-primary m-btn--pill" target="_blank">
                                <i class="la la-eye"></i> Lihat Jadwal
                            </a>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tanggal Pendaftaran</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                   name="action_date" autocomplete="off" type="text" value="{{ Main::format_date($edit->action_time) }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Jam Pendaftaran</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_timepicker_1_modal"
                                   name="action_hours" autocomplete="off" type="text" value="{{ Main::format_time_db($edit->action_time) }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Perbarui Data</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</form>