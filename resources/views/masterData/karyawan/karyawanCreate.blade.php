<form action="{{ route('karyawanInsert') }}" method="post" class="m-form form-send" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Staff </label>
                            <input type="text" class="form-control m-input" name="nama_karyawan">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Posisi Staff </label>
                            <select class="form-control m-input" name="posisi_karyawan">
                                <option value="Dokter">Dokter</option>
                                <option value="Perawat">Perawat</option>
                                <option value="Admin">Admin</option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Telepon Staff </label>
                            <input type="text" class="form-control m-input" name="telp_karyawan">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Poli </label>
                            <input type="text" class="form-control m-input" name="poli">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Jadwal </label>
                            <textarea type="text" class="form-control m-input" name="jadwal"></textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Email Staff </label>
                            <input type="text" class="form-control m-input" name="email_karyawan">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Alamat Staff </label>
                            <textarea class="form-control m-input" name="alamat_karyawan"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-simpan btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
