<form action="{{ route('karyawanUpdate', ['id'=>Main::encrypt($edit->id)]) }}"
      method="post"
      class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Staff </label>
                            <input type="text" class="form-control m-input" name="nama_karyawan"
                                   value="{{ $edit->nama_karyawan }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Posisi Staff </label>
                            <select class="form-control m-input" name="posisi_karyawan">
                                <option value="Dokter" {{ $edit->posisi_karyawan == 'Dokter' ? 'selected':'' }}>Dokter</option>
                                <option value="Perawat" {{ $edit->posisi_karyawan == 'Perawat' ? 'selected':'' }}>Perawat</option>
                                <option value="Admin" {{ $edit->posisi_karyawan == 'Admin' ? 'selected':'' }}>Admin</option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Telepon Staff </label>
                            <input type="text" class="form-control m-input" name="telp_karyawan"
                                   value="{{ $edit->telp_karyawan }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Poli </label>
                            <input type="text" class="form-control m-input" name="poli"
                                   value="{{ $edit->poli}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Jadwal </label>
                            <textarea type="text" class="form-control m-input" name="jadwal">{{ $edit->jadwal }}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Email Staff </label>
                            <input type="text" class="form-control m-input" name="email_karyawan"
                                   value="{{ $edit->email_karyawan }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Alamat Staff </label>
                            <textarea class="form-control m-input"
                                      name="alamat_karyawan">{{ $edit->alamat_karyawan }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
