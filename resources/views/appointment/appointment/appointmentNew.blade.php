<form action="{{ route('appointmentMemberNewInsert') }}"
      method="post"
      class="m-form m-form--fit m-form--label-align-right form-send"
      data-alert-show-success-status="false"
      data-alert-show-success-title="Pendaftaran Terkirim"
      data-alert-show-success-message="Berhasil mengirim Pendaftaran untuk ditindak lanjuti">
    <div class="modal" id="modal-general" role="dialog"
         aria-labelledby="exampleModalLongTitle" aria-hidden="true">

        {{ csrf_field() }}

        <input type="hidden" name="id_patient" value="{{ $edit->id_patient }}">
        <input type="hidden" name="id_appointment" value="{{ $edit->id_appointment }}">

        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="la la-plus"></i> Tambah Pendaftaran
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Keperluan</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input
                                            type="radio"
                                            name="need_type"
                                            value="consult"
                                            {{ $edit->need_type == 'control' ? 'disabled':'' }}
                                            {{ $edit->need_type == 'consult' ? 'checked':'' }}>
                                    Konsultasi
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input
                                            type="radio"
                                            name="need_type"
                                            value="action"
                                            {{ $edit->need_type == 'control' ? 'disabled':'' }}
                                            {{ $edit->need_type == 'action' ? 'checked':'' }}>
                                    Tindakan
                                    <span></span>
                                </label>
                                <label class="m-radio {{ $edit->need_type != 'control' ? 'hidden':'' }}">
                                    <input type="radio"
                                           name="need_type"
                                           value="control"
                                            {{ $edit->need_type == 'control' ? 'checked':'' }}>
                                    Kontrol
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row wrapper-control-step m--hide {{ $edit->need_type == 'control' ? '':'hidden' }}">
                        <label class="col-lg-4 col-form-label">Kontrol ke ?</label>
                        <div class="col-lg-2">
                            <input class="form-control m-input m-input--pill m-touchspin"
                                   name="control_step" value="{{ $edit->control_step ? $edit->control_step : 1 }}"
                                   type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Lokasi Klinik Tujuan</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="location"
                                           value="panjer" {{ $location == 'panjer' ? 'checked':'' }}> Panjer
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="location"
                                           value="jimbaran" {{ $location == 'Jimbaran' ? 'checked':'' }}> Jimbaran
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="location"
                                           value="home_service" {{ $location == 'home_service' ? 'checked':'' }}> Home
                                    Service
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Nomer Rekam Medis</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="medic_record_number" type="text"
                                   value="{{ $edit->patient->medic_record_number }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Nama Pasien</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill" name="name" type="text"
                                   value="{{ $edit->patient->name }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Nama Penanggung Jawab</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="name_responsible" type="text" value="{{ $edit->patient->name_responsible }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Hubungan Pasien dengan Penanggung Jawab</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="name_responsible_relation" type="text"
                                   value="{{ $edit->patient->name_responsible_relation }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tempat Lahir</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="birth_place" type="text" value="{{ $edit->patient->birth_place }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tanggal Lahir</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_datepicker_1_modal" name="birthday"
                                   value="{{ Main::format_date($edit->patient->birthday) }}" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Distrik</label>
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-8">
                                    <select class="form-control m-input m-input--pill m-select2"
                                            name="district"
                                            data-placeholder="Pilih Distrik"
                                            style="width: 100%">
                                        <option value="">Pilih Distrik</option>
                                        @foreach($district as $row)
                                            <option value="{{ $row->dst_name }}" {{ $row->dst_name == $edit->patient->district ? 'selected':'' }}>{{ $row->dst_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <a href="{{ route('districtPage') }}" target="_blank" class="btn btn-success">Tambah
                                        Distrik</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Sub Distrik</label>
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-8">
                                    <select class="form-control m-input m-input--pill m-select2"
                                            name="subdistrict"
                                            data-placeholder="Pilih Sub Distrik"
                                            style="width: 100%">
                                        <option value="">Pilih Sub Distrik</option>
                                        @foreach($subdistrict_2 as $row)
                                            <option value="{{ $row->sdt_name }}" {{ $row->sdt_name == $edit->patient->subdistrict ? 'selected':'' }}>{{ $row->sdt_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <a href="{{ route('subdistrictPage') }}" target="_blank"
                                       class="btn btn-success">Tambah Sub Distrik</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Alamat Detail</label>
                        <div class="col-lg-8">
                            <textarea class="form-control m-input m-input--pill"
                                      name="address"
                                      placeholder="Jln. Gatsu Barat, No 3 Desa Gatsu">{{ $edit->patient->address }}</textarea>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">No HP/WhatsApp</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill" name="phone_1" type="text"
                                   value="{{ $edit->patient->phone_1 }}">
                            <span class="m-form__help">
                                Contoh Pengisian Nomer Telepon: 6281934364063
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Berat Badan</label>
                        <div class="col-lg-2">
                            <input class="form-control m-input m-touchspin m-input--pill"
                                   name="weight" id="m_touchspin_4" value="{{ $edit->patient->weight }}"
                                   type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Suhu Tubuh</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="temperature" type="text" value="{{ $edit->patient->temperature }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Keluhan Pasien</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="complaint" type="text" value="{{ $edit->patient->complaint }}">
                        </div>
                    </div>
{{--                    <div class="form-group m-form__group row">--}}
{{--                        <label class="col-lg-4 col-form-label ">Tipe Poli</label>--}}
{{--                        <div class="col-lg-8">--}}
{{--                            <select name="poli" class="form-control">--}}
{{--                                <option value="Cek Kesehatan" {{ $edit->poli == 'Cek Kesehatan' ? 'selected':'' }}>Cek--}}
{{--                                    Kesehatan--}}
{{--                                </option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Provinsi</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input m-input--pill m-select2"
                                    name="id_province"
                                    style="width: 100%">
                                <option value="">Pilih Provinsi</option>
                                @foreach($province as $row)
                                    <option value="{{ $row->id_province }}" {{ $row->id_province == $edit->patient->id_province ? 'selected':'' }}>{{ $row->province_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Kabupaten</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input m-select2"
                                    name="id_city">
                                <option value="">Pilih Kabupaten</option>
                                @foreach($city as $row)
                                    <option value="{{ $row->id_city }}" {{ $row->id_city == $edit->patient->id_city ? 'selected':'' }}>{{ $row->city_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{--                    <div class="form-group m-form__group row m--hide">--}}
                    {{--                        <label class="col-lg-4 col-form-label">Kecamatan</label>--}}
                    {{--                        <div class="col-lg-8">--}}
                    {{--                            <select class="form-control m-input m-select2"--}}
                    {{--                                    name="id_subdistrict">--}}
                    {{--                                <option value="">Pilih Kecamatan</option>--}}
                    {{--                                @foreach($subdistrict as $row)--}}
                    {{--                                    <option value="{{ $row->id_subdistrict }}" {{ $row->id_subdistrict == $edit->patient->id_subdistrict ? 'selected':'' }}>{{ $row->subdistrict_name }}</option>--}}
                    {{--                                @endforeach--}}
                    {{--                            </select>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Jadwal Kalender</label>
                        <div class="col-lg-8">
                            {{--                            <button type="button" class="btn btn-primary m-btn--pill" data-toggle="modal"--}}
                            {{--                                    data-target="#modal-appointment-schedule">--}}
                            {{--                                <i class="la la-eye"></i> Lihat Jadwal--}}
                            {{--                            </button>--}}
                            <a href="{{ route('scheduleCalendarPage') }}" class="btn btn-primary m-btn--pill"
                               target="_blank">
                                <i class="la la-eye"></i> Lihat Jadwal
                            </a>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Tanggal Pendaftaran</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                   name="appointment_date" autocomplete="off" type="text"
                                   value="{{ Main::format_date($edit->appointment_time) }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Jam Pendaftaran</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_timepicker_1_modal"
                                   name="appointment_hours" autocomplete="off" type="text"
                                   value="{{ Main::format_time_db($edit->appointment_time) }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">No HP Lainnya</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill" name="phone_2" type="text"
                                   value="{{ $edit->patient->phone_2 }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Dokter</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input m-input--pill m-select2"
                                    name="id_karyawan_doctor"
                                    data-placeholder="Pilih Sub Distrik"
                                    style="width: 100%">
                                <option value="">Pilih Dokter</option>
                                @foreach($dokter as $row)
                                    <option value="{{ $row->id }}" {{ $row->id == $edit->patient->id_karyawan_doctor ? 'selected' : '' }}>{{ $row->nama_karyawan.' - '.$row->poli }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Perawat</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input m-input--pill m-select2"
                                    name="id_karyawan_nurse"
                                    data-placeholder="Pilih Sub Distrik"
                                    style="width: 100%">
                                <option value="">Pilih Perawat</option>
                                @foreach($perawat as $row)
                                    <option value="{{ $row->id }}" {{ $row->id == $edit->patient->id_karyawan_nurse ? 'selected':'' }}>{{ $row->nama_karyawan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Perbarui Data</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</form>
