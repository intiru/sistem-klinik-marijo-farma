<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 12px;
            font-weight: bold;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }

        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }

        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }

        .bold {
            font-weight: bold;
        }


    </style>
    <?php
    $width_label = '64';
    ?>

</head>
<body>
<div class="text-center">
    <img src="{{ asset('images/app-logo.jpeg') }}" width="300">

    @if($dokter)
        <h2><strong>Dokter: {{ $dokter->nama_karyawan }}</strong></h2>
    @endif
    @if($perawat)
        <h2><strong>Perawat: {{ $perawat->nama_karyawan }}</strong></h2>
    @endif
    <h2>Poli: {{ $appointment->patient->poli }}</h2>
    <br />
    <h2>Pasien: {{ $appointment->patient->name }}</h2>
    <h1>NOMER ANTRIAN</h1>
    <h1>{{ $no_antrian }}</h1>
    <h2>{{ \app\Helpers\Main::format_datetime($appointment->appointment_time) }}</h2>

</div>

</body>
