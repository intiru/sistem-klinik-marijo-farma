<form action="{{ route('appointmentMemberNewInsert') }}"
      method="post"

      data-alert-show="true"
      data-alert-field-message="true"

      data-alert-show-success-status="false"
      data-alert-show-success-title="Appointment Terkirim"
      data-alert-show-success-message="Berhasil mengirim appointment untuk ditindak lanjuti"
      class="m-form m-form--fit m-form--label-align-right form-send">

    {{ csrf_field() }}
    <input type="hidden" name="via" value="admin">

    <div class="modal" id="modal-appointment-create" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="la la-plus"></i> Tambah Pendaftaran
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Keperluan</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="need_type"
                                           value="consult"> Konsultasi
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="need_type"
                                           value="action" checked> Tindakan
                                    <span></span>
                                </label>
{{--                                <label class="m-radio">--}}
{{--                                    <input type="radio" name="need_type"--}}
{{--                                           value="control"> Kontrol--}}
{{--                                    <span></span>--}}
{{--                                </label>--}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row wrapper-control-step hidden">
                        <label class="col-lg-4 col-form-label">Kontrol ke ?</label>
                        <div class="col-lg-2">
                            <input class="form-control m-input m-input--pill"
                                   name="control_step" id="m_touchspin_4" value=""
                                   type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Lokasi Klinik Tujuan</label>
                        <div class="col-lg-8">
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="location"
                                           value="panjer" checked> Panjer
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="location"
                                           value="jimbaran"> Jimbaran
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="location"
                                           value="home_service"> Home Service
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Nomer Rekam Medis</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="medic_record_number" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Nama Pasien</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="name" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Nama Penanggung Jawab</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="name_responsible" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Hubungan Pasien dengan Penanggung Jawab</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="name_responsible_relation" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tempat Lahir</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="birth_place" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Tanggal Lahir</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                   name="birthday" type="text" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Distrik</label>
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-8">
                                    <select class="form-control m-input m-input--pill m-select2"
                                            name="district"
                                            data-placeholder="Pilih Distrik"
                                            style="width: 100%">
                                        <option value="">Pilih Distrik</option>
                                        @foreach($district as $row)
                                            <option value="{{ $row->dst_name }}">{{ $row->dst_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <a href="{{ route('districtPage') }}" target="_blank" class="btn btn-success">Tambah Distrik</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Sub Distrik</label>
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-8">
                                    <select class="form-control m-input m-input--pill m-select2"
                                            name="subdistrict"
                                            data-placeholder="Pilih Sub Distrik"
                                            style="width: 100%">
                                        <option value="">Pilih Sub Distrik</option>
                                        @foreach($subdistrict as $row)
                                            <option value="{{ $row->sdt_name }}">{{ $row->sdt_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <a href="{{ route('subdistrictPage') }}" target="_blank" class="btn btn-success">Tambah Sub Distrik</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Alamat Detail</label>
                        <div class="col-lg-8">
                            <textarea class="form-control m-input m-input--pill"
                                      name="address" placeholder="Jln. Gatsu Barat, No 3 Desa Gatsu"></textarea>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">No HP/WhatsApp</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill" name="phone_1" type="text">
                            <span class="m-form__help">
{{--                                Contoh Pengisian Nomer Telepon: 6281934364063--}}
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Berat Badan</label>
                        <div class="col-lg-2">
                            <input class="form-control m-input m-input--pill"
                                   name="weight" id="m_touchspin_4" value="55"
                                   type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Suhu Tubuh</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="temperature" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Keluhan Pasien</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill"
                                   name="complaint" type="text">
                        </div>
                    </div>
{{--                    <div class="form-group m-form__group row">--}}
{{--                        <label class="col-lg-4 col-form-label">Tipe Poli</label>--}}
{{--                        <div class="col-lg-8">--}}
{{--                            <select name="poli" class="form-control">--}}
{{--                                <option value="Cek Kesehatan">Cek Kesehatan</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Provinsi</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input m-input--pill m-select2"
                                    name="id_province"
                                    data-placeholder="Pilih Provinsi"
                                    style="width: 100%">
                                <option value="">Pilih Provinsi</option>
                                @foreach($province as $row)
                                    <option value="{{ $row->id_province }}">{{ $row->province_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Kabupaten</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input"
                                    name="id_city">
                                <option value="">Pilih Kabupaten</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Kecamatan</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input"
                                    name="id_subdistrict">
                                <option value="">Pilih Kecamatan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Jadwal Kalender</label>
                        <div class="col-lg-8">
                            <a href="{{ route('scheduleCalendarPage') }}" class="btn btn-primary m-btn--pill" target="_blank">
                                <i class="la la-eye"></i> Lihat Jadwal
                            </a>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Tanggal Pendaftaran</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                   name="appointment_date" autocomplete="off" type="text" value="{{ date('d-m-Y') }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">Jam Pendaftaran</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill m_timepicker_1_modal"
                                   name="appointment_hours" autocomplete="off" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--hide">
                        <label class="col-lg-4 col-form-label">No HP Lainnya</label>
                        <div class="col-lg-8">
                            <input class="form-control m-input m-input--pill" name="phone_2" type="text">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Dokter</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input m-input--pill m-select2"
                                    name="id_karyawan_doctor"
                                    data-placeholder="Pilih Sub Distrik"
                                    style="width: 100%">
                                <option value="">Pilih Dokter</option>
                                @foreach($dokter as $row)
                                    <option value="{{ $row->id }}">{{ $row->nama_karyawan. ' - Poli '.$row->poli }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">Perawat</label>
                        <div class="col-lg-8">
                            <select class="form-control m-input m-input--pill m-select2"
                                    name="id_karyawan_nurse"
                                    data-placeholder="Pilih Sub Distrik"
                                    style="width: 100%">
                                <option value="">Pilih Perawat</option>
                                @foreach($perawat as $row)
                                    <option value="{{ $row->id }}">{{ $row->nama_karyawan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>

</form>
