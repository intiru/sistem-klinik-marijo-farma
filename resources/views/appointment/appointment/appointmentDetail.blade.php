<div class="modal" id="modal-general" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">

    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <i class="la la-eye"></i> Detail Pendaftaran
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Keperluan</label>
                    <div class="col-lg-8 ">
                        {{ Main::need_type_label($row->need_type) }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Nama</label>
                    <div class="col-lg-8 ">
                        {{ $row->patient->name }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Umur</label>
                    <div class="col-lg-8 ">
                        {{ Main::format_age($row->patient->birthday) }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Tanggal Lahir</label>
                    <div class="col-lg-8 ">
                        {{ Main::format_date_label($row->patient->birthday) }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Provinsi</label>
                    <div class="col-lg-8 ">
                        {{ $row->patient->province->province_name }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Kabupaten</label>
                    <div class="col-lg-8 ">
                        {{ $row->patient->city->city_name }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Kecamatan</label>
                    <div class="col-lg-8 ">
                        {{ $row->patient->subdistrict->subdistrict_name }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Alamat</label>
                    <div class="col-lg-8 ">
                        {{ $row->patient->address }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Berat Badan</label>
                    <div class="col-lg-8">
                        {{ $row->patient->weight }} KG
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Waktu Pendaftaran</label>
                    <div class="col-lg-8 ">
                        {{ Main::format_datetime($row->appointment_time) }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Lokasi</label>
                    <div class="col-lg-8 ">
                        @php
                            $location = '';
                                if($row->need_type == 'consult') {
                                    $location = \app\Models\mConsult::where('id_consult', $row->id_consult)->value('consult_location');
                                } else if($row->need_type == 'action') {
                                    $location = \app\Models\mAction::where('id_action', $row->id_action)->value('action_location');
                                } else if($row->need_type == 'control') {
                                    $location = \app\Models\mControl::where('id_control', $row->id_control)->value('control_location');
                                }
                        @endphp

                        {{ ucwords($location) }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">No HP/WhatsApp</label>
                    <div class="col-lg-8 ">
                        {{ $row->patient->phone_1 }}
                    </div>
                </div>
                <div class="form-group m-form__group row m--hide">
                    <label class="col-lg-4 col-form-label">No HP Lainnya</label>
                    <div class="col-lg-8 ">
                        {{ $row->patient->phone_2 }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>