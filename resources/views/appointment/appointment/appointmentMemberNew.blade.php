<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        Pendaftaran Rumah Sunat Bali
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <link href="{{ asset("assets/vendors/base/vendors.bundle.css") }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("assets/demo/default/base/style.bundle.css") }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("css/loading.css") }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="{{ asset('images/app-logo.png') }}"/>
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">

    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <div class="m-content">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__body m-portlet__body--no-padding">
                        <div class="m-pricing-table-2">
                            <div class="m-pricing-table-2__head">
                                <div class="m-pricing-table-2__title m--font-light">
                                    <img src="{{ asset('images/rumah-sunat-bali-logo.png') }}">
                                    <h1>
                                        Pendaftaran {{ env('BUSINESS_NAME') }}
                                    </h1>
                                </div>

                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="m-pricing-table_content1" aria-expanded="true">
                                    <div class="m-pricing-table-2__content">
                                        <div class="m-pricing-table-2__container">
                                            <div class=" row">
                                                <div class="m-pricing-table-2__item m-pricing-table-2__items col-lg-6 offset-lg-3">
                                                    <form action="{{ route('appointmentMemberNewInsert') }}"
                                                          method="post"

                                                          data-alert-show="true"
                                                          data-alert-field-message="true"

                                                          data-alert-show-success-status="true"
                                                          data-alert-show-success-title="Appointment Terkirim"
                                                          data-alert-show-success-message="Berhasil mengirim appointment untuk ditindak lanjuti"
                                                          class="m-form m-form--fit m-form--label-align-right form-send">
                                                        {{ csrf_field() }}

                                                        <input type="hidden" name="via" value="user">

                                                        <div class="m-portlet__body">
                                                            <div class="form-group m-form__group m--margin-top-10">
                                                                <div class="alert m-alert m-alert--default"
                                                                     role="alert">
                                                                    Mohon untuk mengisi form dibawah untuk melakukan
                                                                    perjanjian konsultasi di {{ env('BUSINESS_NAME') }}
                                                                </div>
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <div class="col-12 font-italic">
                                                                    <span class="required-label">*</span>) diperlukan
                                                                </div>
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label for="exampleInputEmail1" class="required">
                                                                    Keperluan
                                                                </label>
                                                                <div class="m-radio-inline">
                                                                    <label class="m-radio">
                                                                        <input type="radio" name="need_type"
                                                                               value="consult" checked> Konsultasi
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="m-radio">
                                                                        <input type="radio" name="need_type"
                                                                               value="action"> Tindakan
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label for="exampleInputEmail1" class="required">
                                                                    Lokasi Klinik Tujuan
                                                                </label>
                                                                <div class="m-radio-inline">
                                                                    <label class="m-radio">
                                                                        <input type="radio" name="location"
                                                                               value="panjer" checked> Panjer
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="m-radio">
                                                                        <input type="radio" name="location"
                                                                               value="jimbaran"> Jimbaran
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="m-radio">
                                                                        <input type="radio" name="location"
                                                                               value="home_service"> Home Service
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label for="exampleInputEmail1" class="required">
                                                                    Nama
                                                                </label>
                                                                <input class="form-control m-input m-input--pill"
                                                                       name="name" type="text">
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label for="exampleInputEmail1" class="required">
                                                                    Tanggal Lahir
                                                                </label>
                                                                <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                                                       name="birthday" autocomplete="off" type="text">
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label for="exampleInputEmail1" class="required">
                                                                    Provinsi
                                                                </label>
                                                                <select class="form-control m-input m-input--pill m-select2"
                                                                        id="m_select2_1"
                                                                        name="id_province"
                                                                        data-placeholder="Pilih Provinsi">
                                                                    <option value="">Pilih Provinsi</option>
                                                                    @foreach($province as $row)
                                                                        <option value="{{ $row->id_province }}">{{ $row->province_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label for="exampleInputEmail1" class="required">
                                                                    Kabupaten
                                                                </label>
                                                                <select class="form-control m-input"
                                                                        name="id_city">
                                                                    <option value="">Pilih Kabupaten</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label for="exampleInputEmail1" class="required">
                                                                    Kecamatan
                                                                </label>
                                                                <select class="form-control m-input"
                                                                        name="id_subdistrict">
                                                                    <option value="">Pilih Kecamatan</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label for="exampleInputEmail1" class="required">
                                                                    Alamat Detail
                                                                </label>
                                                                <textarea class="form-control m-input m-input--pill"
                                                                          name="address" placeholder="Jln. Gatsu Barat, No 3 Desa Gatsu"></textarea>
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label for="exampleInputEmail1" class="required">
                                                                    Berat Badan
                                                                </label>
                                                                <input class="form-control m-input m-input--pill"
                                                                       name="weight" id="m_touchspin_4" value="55"
                                                                       type="text">
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label>Tanggal Pendaftaran</label>
                                                                    <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                                                           name="appointment_date" autocomplete="off" type="text" value="{{ date('d-m-Y') }}">
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label>Jam Pendaftaran</label>
                                                                    <input class="form-control m-input m-input--pill m_timepicker_1_modal"
                                                                           name="appointment_hours" autocomplete="off" type="text">
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label for="exampleInputEmail1" class="required">
                                                                    No HP/WhatsApp
                                                                </label>
                                                                <input class="form-control m-input m-input--pill"
                                                                       name="phone_1" type="text">
                                                                <span class="m-form__help">
                                                                    Contoh Pengisian Nomer Telepon: 6281934364063
                                                                </span>
                                                            </div>
                                                            <div class="form-group m-form__group">
                                                                <label for="exampleInputEmail1">
                                                                    No HP Lainnya
                                                                </label>
                                                                <input class="form-control m-input m-input--pill"
                                                                       name="phone_2" type="text">
                                                            </div>


                                                        </div>
                                                        <div class="m-portlet__foot m-portlet__foot--fit">
                                                            <div class="m-form__actions text-center">
                                                                <button type="submit"
                                                                        class="btn btn-success m-btn--pill">
                                                                    Buat Pendaftaran
                                                                </button>
{{--                                                                <a href="{{ \app\Helpers\Main::$website_official }}"--}}
{{--                                                                   target="_blank" class="btn btn-info m-btn--pill">--}}
{{--                                                                    Lihat Website Official--}}
{{--                                                                </a>--}}
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>

</div>

<span id="base-value"
      class="hidden"
      data-route-city-list="{{ route('cityList') }}"
      data-route-subdistrict-list="{{ route('subdistrictList') }}"
      data-csrf-token="{{ csrf_token() }}"></span>


<div class='container-loading hidden'>
    <div class='loader'>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--text'></div>
        <div class='loader--desc'></div>
    </div>
</div>


<script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/select2.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
<!--end::Base Scripts -->
</body>
<!-- end::Body -->
</html>

