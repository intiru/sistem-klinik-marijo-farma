@extends(\Illuminate\Support\Facades\Session::get('user') ? '../general/index': '../general/index_public')


@section('css')

@endsection

@section('js')

@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet akses-list">
                <div class="m-portlet__body">
                    <table class="table m-table m-table--head-bg-success">
                        <thead>
                        <tr>
                            <th>
                                No
                            </th>
                            <th>
                                Poliklinik
                            </th>
                            <th>
                                Nama Dokter
                            </th>
                            <th>
                                Jadwal
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dokter as $key => $row)
                            <tr>
                                <th scope="row">
                                    {{ ++$key }}
                                </th>
                                <td>
                                    {{ $row->poli }}
                                </td>
                                <td>
                                    {{ $row->nama_karyawan }}
                                </td>
                                <td>
                                    {{ $row->jadwal }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>
@endsection
