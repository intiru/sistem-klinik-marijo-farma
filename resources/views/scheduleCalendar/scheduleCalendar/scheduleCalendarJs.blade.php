<script type="text/javascript">
    var CalendarBasic = function() {

        return {
            //main function to initiate the module
            init: function() {
                var todayDate = moment().startOf('day');
                var YM = todayDate.format('YYYY-MM');
                var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
                var TODAY = todayDate.format('YYYY-MM-DD');
                var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

                $('#m_calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay,listWeek'
                    },
                    height: "auto",
                    // editable: true,
                    // eventLimit: true, // allow "more" link when too many events
                    navLinks: true,
                    events: [
                        @foreach($appointment as $row)
                            @php
                            $patient = \app\Models\mPatient::where('id_patient', $row->id_patient)->first();
                            $location = '';
                                if($row->need_type == 'consult') {
                                    $location = \app\Models\mConsult::select('consult_location')->where('id_consult', $row->id_consult)->value('consult_location');
                                }
                                if($row->need_type == 'action') {
                                    $location = \app\Models\mAction::select('action_location')->where('id_action', $row->id_action)->value('action_location');
                                }
                                if($row->need_type == 'control') {
                                    $location = \app\Models\mControl::select('control_location')->where('id_control', $row->id_control)->value('control_location');
                                }
                            @endphp
                        {
                            title: '{{ Main::need_type_label($row->need_type, $row->control_step) }} - {{ $patient ? $patient['name'] : '' }}',
{{--                            description: '{{ Main::need_type_label($row->need_type, $row->control_step) }} - {{ $patient ? $patient['name'] : '' }} - {{ ucwords($location) }} - {{ $patient ? $patient['weight'].' Kg' : '' }} - {{ Main::format_time_label($row->appointment_time) }}',--}}
                            className: "{{ Main::need_type_class($row->need_type) }}",
                            start: '{{ $row->appointment_time }}'
                        },
                        @endforeach
                    ],

                    eventRender: function(event, element) {
                        if (element.hasClass('fc-day-grid-event')) {
                            element.data('content', event.description);
                            element.data('placement', 'top');
                            mApp.initPopover(element);
                        } else if (element.hasClass('fc-time-grid-event')) {
                            element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>');
                        } else if (element.find('.fc-list-item-title').lenght !== 0) {
                            element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>');
                        }
                    }
                });
            }
        };
    }();

    jQuery(document).ready(function() {
        CalendarBasic.init();
    });
</script>