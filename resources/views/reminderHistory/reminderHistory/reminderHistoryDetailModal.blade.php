<div class="modal" id="modal-general" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <i class="la la-info"></i> Detail Reminder History
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Judul Pesan</label>
                    <div class="col-lg-8">
                        {{ $reminder_history->title }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Ke Pasien</label>
                    <div class="col-lg-8">
                        @php
                            if($reminder_history['appointment']['patient']['name']) {
                                $name = $reminder_history['appointment']['patient']['name'];
                            } elseif($reminder_history['patient']['name']) {
                                $name = $reminder_history['patient']['name'];
                            } else {
                                $name = 'Admin';
                            }
                        @endphp

                        {{ $name }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Phone/Email</label>
                    <div class="col-lg-8">
                        {{ $reminder_history->phone ? $reminder_history->phone : $reminder_history->email }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Isi Pesan</label>
                    <div class="col-lg-8">
                        {!! $reminder_history->message !!}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Waktu Pengiriman</label>
                    <div class="col-lg-8">
                        {{ $reminder_history->created_at }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
