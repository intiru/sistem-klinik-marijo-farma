@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('userList')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet ">
                <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 col-sm-12">Rentang Tanggal Pendaftaran</label>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       class="form-control m_datepicker_1_modal"
                                       readonly
                                       value="{{ $date_from }}"
                                       placeholder="Select time"
                                       name="date_from"/>
                            </div>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       class="form-control m_datepicker_1_modal"
                                       readonly
                                       value="{{ $date_to }}"
                                       placeholder="Select time"
                                       name="date_to"/>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <button type="submit" class="btn btn-accent m-btn--pill btn-sm">
                            <i class="la la-search"></i> Filter Data
                        </button>
                    </div>
                </form>
            </div>

            <div class="m-portlet akses-list">
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <table class="akses-list table table-bordered datatable-new"
                               data-url="{{ route('reminderHistoryDataTable') }}"
                               data-date-from="{{ $date_from }}"
                               data-date-to="{{ $date_to }}"
                               data-column="{{ json_encode($datatable_column) }}">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th class="no-sort">Judul</th>
                                <th class="no-sort">Ke Pasien</th>
                                <th class="no-sort">Isi Pesan</th>
                                <th class="no-sort">Waktu Pengiriman</th>
                                <th class="no-sort">Menu</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
