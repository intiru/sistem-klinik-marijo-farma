<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">

        @page { margin: 6px 15px; }

        body {
            font-size: 11px;
            margin: 2px 15px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 18px;
            font-weight: bold;
            text-decoration: underline;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }

        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }

        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }

        .bold {
            font-weight: bold;
        }

        .sub-1 {
            padding-left: 20px;
        }
        .sub-2 {
            padding-left: 40px;
        }


    </style>
    <?php
    $width_label = '64';
    ?>

</head>
<body>
<div class="text-center">
    <img src="{{ asset('images/app-logo.jpeg') }}" width="300">
</div>
<h1 align="center">RESUME MEDIS PASIEN</h1>
<hr/>

<div class="div-2"></div>
<div class="div-2 text-right">
    Lospalos Central, {{ date('d F Y') }}<br/>
    No. RM : {{ $patient->medic_record_number }}
</div>
<div class="clearfix"></div>
<br />
<h3>Identitas Pasien</h3>
<div class="sub-1">
    <table>
        <tr>
            <td width="120">Nama</td>
            <td width="8">:</td>
            <td>{{ $patient->name }}</td>
        </tr>
        <tr>
            <td>Distrik</td>
            <td>:</td>
            <td>{{ $patient->district }}</td>
        </tr>
        <tr>
            <td>Subdistrik</td>
            <td>:</td>
            <td>{{ $patient->subdistrict }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td>{{ $patient->address }}</td>
        </tr>
        <tr>
            <td>No. HP/WhatsApp</td>
            <td>:</td>
            <td>{{ $patient->phone_1 }}</td>
        </tr>
        <tr>
            <td>No. HP Lainnya</td>
            <td>:</td>
            <td>{{ $patient->phone_2 }}</td>
        </tr>
        <tr>
            <td>Umur</td>
            <td>:</td>
            <td>{{ \app\Helpers\Main::format_age($patient->birthday) }}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir</td>
            <td>:</td>
            <td>{{ Main::format_date_label($patient->birthday) }}</td>
        </tr>
    </table>
</div>
<br />
<h3>Anamnesa</h3>
<div class="sub-1">
    <table>
        <tr>
            <td width="120">Diagnosa</td>
            <td width="8">:</td>
            <td>{{ $patient->medic_record->diagnosa }}</td>
        </tr>
        <tr>
            <td width="120">Riwayat Penyakit Sekarang</td>
            <td width="8">:</td>
            <td>{{ $patient->medic_record->disease_now }}</td>
        </tr>
        <tr>
            <td>Riwayat Penyakit Sebelumnya</td>
            <td>:</td>
            <td>{{ $patient->medic_record->disease_before }}</td>
        </tr>
        <tr>
            <td>Riwayat Pengobatan</td>
            <td>:</td>
            <td>{{ $patient->medic_record->medical_history }}</td>
        </tr>
        <tr>
            <td>Riwayat Alergi</td>
            <td>:</td>
            <td>{{ $patient->medic_record->alergy_history }}</td>
        </tr>
    </table>
</div>
<br />
<h3>Pemeriksaan Fisik</h3>
<div class="sub-1">
    <table>
        <tr>
            <td width="120">Vital Sign - Tensi</td>
            <td width="8">:</td>
            <td>{{ $patient->medic_record->sign_tension }}</td>
        </tr>
        <tr>
            <td>Vital Sign - Temperatur</td>
            <td>:</td>
            <td>{{ $patient->medic_record->sign_temp }}</td>
        </tr>
        <tr>
            <td>Vital Sign - Nadi</td>
            <td>:</td>
            <td>{{ $patient->medic_record->sign_pulse }}</td>
        </tr>
        <tr>
            <td>Vital Sign - RR</td>
            <td>:</td>
            <td>{{ $patient->medic_record->sign_rr }}</td>
        </tr>
        <tr>
            <td>Keadaan Umum</td>
            <td>:</td>
            <td>{{ $patient->medic_record->general_condition }}</td>
        </tr>
        <tr>
            <td valign="top">Pemeriksaan Genital</td>
            <td valign="top">:</td>
            <td valign="top">{{ $patient->medic_record->genital_check }}</td>
        </tr>
    </table>
</div>
<br />
<h3>Assessment</h3>
<p>{{ $patient->medic_record->assessment }}</p>
<br />
<h3>Planning</h3>
{{ $patient->medic_record->planning }}
<br /><br /><br />
<div class="div-3"></div>
<div class="div-3"></div>
<div class="text-center div-3">
    <strong>Dokter Pemeriksa,</strong><br/>
    <br/>
    <br/>
    <br/>
    <strong>{{ $patient->doctor->nama_karyawan }}</strong><br />
</div>
<div class="clearfix"></div>


</body>
