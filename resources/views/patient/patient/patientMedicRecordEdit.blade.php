@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('userList')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">


            <form action="{{ route('patientMedicRecordUpdate', ['id_patient' => Main::encrypt($patient->id_patient)]) }}"
                  method="post"
                  class="form-send"
                  data-alert-show="true"
                  data-alert-field-message="true">

                {{ csrf_field() }}
                <div class="m-portlet akses-list">
                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-form__section m-form__section--first">
                                <div class="m-form__heading">
                                    <h3 class="m-form__heading-title">
                                        Identitas Pasien :
                                    </h3>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Nama
                                    </label>
                                    <div class="col-lg-10">
                                        <input class="form-control m-input m-input--pill"
                                               name="name" type="text" value="{{ $patient->name }}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Tanggal Lahir
                                    </label>
                                    <div class="col-lg-10">
                                        <input class="form-control m-input m-input--pill m_datepicker_1_modal"
                                               name="birthday" readonly type="text"
                                               value="{{ Main::format_date($patient->birthday) }}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Provinsi</label>
                                    <div class="col-lg-10">
                                        <select class="form-control m-input m-input--pill m-select2"
                                                name="id_province"
                                                data-placeholder="Pilih Provinsi"
                                                style="width: 100%">
                                            <option value="">Pilih Provinsi</option>
                                            @foreach($province as $row)
                                                <option value="{{ $row->id_province }}" {{ $row->id_province == $patient->id_province ? 'selected':'' }}>
                                                    {{ $row->province_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Kabupaten</label>
                                    <div class="col-lg-10">
                                        <select class="form-control m-input m-select2"
                                                name="id_city">
                                            <option value="">Pilih Kabupaten</option>
                                            @foreach($city as $row)
                                                <option value="{{ $row->id_city }}" {{ $row->id_city == $patient->id_city ? 'selected':'' }}>
                                                    {{ $row->city_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Kecamatan</label>
                                    <div class="col-lg-10">
                                        <select class="form-control m-input m-select2"
                                                name="id_subdistrict">
                                            <option value="">Pilih Kecamatan</option>
                                            @foreach($subdistrict as $row)
                                                <option value="{{ $row->id_subdistrict }}" {{ $row->id_subdistrict == $patient->id_subdistrict ? 'selected':'' }}>
                                                    {{ $row->subdistrict_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Alamat Detail</label>
                                    <div class="col-lg-10">
                            <textarea class="form-control m-input m-input--pill"
                                      name="address"
                                      placeholder="Jln. Gatsu Barat, No 3 Desa Gatsu">{{ $patient->address }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">Berat Badan</label>
                                    <div class="col-lg-2">
                                        <input class="form-control m-input m-input--pill"
                                               name="weight" id="m_touchspin_4" value="{{ $patient->weight }}"
                                               type="text">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">No HP/WhatsApp</label>
                                    <div class="col-lg-10">
                                        <input class="form-control m-input m-input--pill" name="phone_1" value="{{ $patient->phone_1 }}" type="text">
                                        <span class="m-form__help">
                                            Contoh Pengisian Nomer Telepon: 6281934364063
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">No HP Lainnya</label>
                                    <div class="col-lg-10">
                                        <input class="form-control m-input m-input--pill" name="phone_2" value="{{ $patient->phone_2 }}" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <div class="m-form__section m-form__section">
                                <div class="m-form__heading">
                                    <h3 class="m-form__heading-title">
                                        Anamnesa :
                                    </h3>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label required">
                                        Riwayat Penyakit Sekarang
                                    </label>
                                    <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="disease_now">{{ isset($patient->medic_record->disease_now) ? $patient->medic_record->disease_now : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label required">
                                        Riwayat Penyakit Sebelumnya
                                    </label>
                                    <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="disease_before">{{ isset($patient->medic_record->disease_before) ? $patient->medic_record->disease_before : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label required">
                                        Riwayat Pengobatan
                                    </label>
                                    <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="medical_history">{{ isset($patient->medic_record->medical_history) ? $patient->medic_record->medical_history : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label required">
                                        Riwayat Alergi
                                    </label>
                                    <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="alergy_history">{{ isset($patient->medic_record->alergy_history) ? $patient->medic_record->alergy_history : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <div class="m-form__section m-form__section">
                                <div class="m-form__heading">
                                    <h3 class="m-form__heading-title">
                                        Pemeriksaan Fisik :
                                    </h3>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label required">
                                        Vital Sign - Tensi
                                    </label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control m-input m-input--pill"
                                               name="sign_tension"
                                               value="{{ isset($patient->medic_record->sign_tension) ? $patient->medic_record->sign_tension : '' }}">
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label required">
                                        Vital Sign - Temperatur
                                    </label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control m-input m-input--pill"
                                               name="sign_temp"
                                               value="{{ isset($patient->medic_record->sign_temp) ? $patient->medic_record->sign_temp : '' }}">
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label required">
                                        Vital Sign - Nadi
                                    </label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control m-input m-input--pill"
                                               name="sign_pulse"
                                               value="{{ isset($patient->medic_record->sign_pulse) ? $patient->medic_record->sign_pulse : '' }}">
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label required">
                                        Vital Sign - RR
                                    </label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control m-input m-input--pill"
                                               name="sign_rr"
                                               value="{{ isset($patient->medic_record->sign_rr) ? $patient->medic_record->sign_rr : '' }}">
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label required">
                                        Keadaan Umum
                                    </label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control m-input m-input--pill"
                                               name="general_condition"
                                               value="{{ isset($patient->medic_record->general_condition) ? $patient->medic_record->general_condition : '' }}">
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label required">
                                        Pemeriksaan Genital
                                    </label>
                                    <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="genital_check">{{ isset($patient->medic_record->genital_check) ? $patient->medic_record->genital_check : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <div class="m-form__section m-form__section">
                                <div class="m-form__heading">
                                    <h3 class="m-form__heading-title required">
                                        Assessment :
                                    </h3>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label">

                                    </label>
                                    <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="assessment">{{ isset($patient->medic_record->assessment) ? $patient->medic_record->assessment : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <div class="m-form__section m-form__section--last">
                                <div class="m-form__heading">
                                    <h3 class="m-form__heading-title required">
                                        Planning :
                                    </h3>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label">

                                    </label>
                                    <div class="col-lg-10">
                                            <textarea class="form-control m-input m-input--pill"
                                                      name="planning">{{ isset($patient->medic_record->planning) ? $patient->medic_record->planning : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons ">
                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <i class="la la-check"></i> Simpan
                    </button>

                    <a href="{{ route('patientList') }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <i class="la la-angle-double-left"></i>
                        Kembali ke Daftar
                    </a>
                </div>

            </form>

        </div>

    </div>
@endsection
