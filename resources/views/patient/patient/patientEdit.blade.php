<div class="modal" id="modal-patient-edit" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <i class="la la-pencil"></i> Edit Pasien
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Nama</label>
                    <div class="col-lg-8">
                        <input class="form-control m-input m-input--pill" type="text">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Tanggal Lahir</label>
                    <div class="col-lg-8">
                        <input class="form-control m-input m-input--pill m_datepicker_1_modal" readonly type="text">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Alamat</label>
                    <div class="col-lg-8">
                        <textarea class="form-control m-input m-input--pill"></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Berat Badan</label>
                    <div class="col-lg-2">
                        <input class="form-control m-input m-input--pill" id="m_touchspin_4" value="34" type="text">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">No HP/WhatsApp</label>
                    <div class="col-lg-8">
                        <input class="form-control m-input m-input--pill" type="text">
                        <span class="m-form__help">
                                Contoh Pengisian Nomer Telepon: 6281934364063
                            </span>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">No HP Lainnya</label>
                    <div class="col-lg-8">
                        <input class="form-control m-input m-input--pill" type="text">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Perbarui Data</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
