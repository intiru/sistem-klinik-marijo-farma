<div class="modal" id="modal-patient-detail" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content detail-info">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <i class="la la-info"></i> Detail Pasien
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Nama</label>
                    <div class="col-lg-8">
                        I Putu Mahendra Adi Wardana
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Umur</label>
                    <div class="col-lg-8">
                        29 Tahun
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Tanggal Lahir</label>
                    <div class="col-lg-8">
                        11 Desember 1991
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Alamat</label>
                    <div class="col-lg-8">
                        Jln. Gadung 1 No 3, Desa Sibangkaja, Abiansemal, Badung
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">Berat Badan</label>
                    <div class="col-lg-2">
                        77 KG
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">No HP/WhatsApp</label>
                    <div class="col-lg-8">
                        081 934 364 063
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-4 col-form-label">No HP Lainnya</label>
                    <div class="col-lg-8">
                        081 934 364 063
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
