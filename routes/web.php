<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


Route::get('/error-test', function () {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function () {
    Artisan::call('down');
});

Route::get('/production', function () {
    Artisan::call('up');
});

Route::post('/submit-form', function () {
    //
})->middleware(\Spatie\HttpLogger\Middlewares\HttpLogger::class);

Route::group(['namespace' => 'Appointment'], function () {

    Route::group(['middleware'=> 'authLogin'], function() {
        Route::get('/pendaftaran', 'Appointment@index')->name('appointmentList');
        Route::post('/pendaftaran/datatable', 'Appointment@data_table')->name('appointmentDataTable');
        Route::get('/pendaftaran/detail/{id_appointment}', 'Appointment@detail')->name('appointmentDetail');
        Route::get('/pendaftaran/edit/{id_appointment}', 'Appointment@edit')->name('appointmentEdit');
        Route::post('/pendaftaran/update/{id_appointment}', 'Appointment@update')->name('appointmentUpdate');
        Route::delete('/pendaftaran/delete/{id_appointment}', 'Appointment@delete')->name('appointmentDelete');
        Route::get('/pendaftaran/cetak/{id_appointment}', 'Appointment@queue_print')->name('appointmentQueuePrint');

        Route::get('/pendaftara/baru/{id_appointment}', 'Appointment@appointment')->name('appointmentNew');

        Route::get('/pendaftaran/list', 'Appointment@index')->name('appointmentListData');
    });



    Route::get('/appointment/member/new', 'Appointment@member_new')->name('appointmentMemberNew');
    Route::post('/appointment/member/new', 'Appointment@member_new_insert')->name('appointmentMemberNewInsert');

});

Route::group(['namespace' => 'Consult', 'middleware' => 'authLogin'], function () {

    Route::get('/consult', 'Consult@index')->name('consultList');
    Route::post('/consult/datatable', 'Consult@data_table')->name('consultDataTable');
    Route::get('/consult/edit/{id_consult}', 'Consult@edit')->name('consultEdit');
    Route::post('/consult/update/{id_consult}', 'Consult@update')->name('consultUpdate');
    Route::get('/consult/detail/{id_consult}', 'Consult@detail')->name('consultDetail');
    Route::get('/consult/done/modal/{id_consult}', 'Consult@done_modal')->name('consultDoneModal');
    Route::post('/consult/done/process/{id_consult}', 'Consult@done_process')->name('consultDoneProcess');
    Route::get('/consult/cancel/modal/{id_consult}', 'Consult@cancel_modal')->name('consultCancelModal');
    Route::post('/consult/cancel/process/{id_consult}', 'Consult@cancel_process')->name('consultCancelProcess');

});

Route::group(['namespace' => 'Action', 'middleware' => 'authLogin'], function () {

    Route::get('/action/process/list', 'Action@index_process')->name('actionProcessList');
    Route::get('/action/done/list', 'Action@index_done')->name('actionDoneList');

    Route::post('/action/datatable/process', 'Action@process_data_table')->name('actionProcessDataTable');
    Route::post('/action/datatable/done', 'Action@done_data_table')->name('actionDoneDataTable');

    Route::get('/action/done/page/{id_action}', 'Action@done_page')->name('actionDonePage');
    Route::post('/action/done/process/{id_action}', 'Action@done_process')->name('actionDoneProcess');
    Route::get('/action/cancel/{id_action}', 'Action@cancel_process')->name('actionCancel');
    Route::get('/action/back/{id_action}', 'Action@back_process')->name('actionBack');

    Route::get('/action/edit/process/modal/{id_action}', 'Action@edit_process_modal')->name('actionEditProcessModal');
    Route::get('/action/edit/done/modal/{id_action}', 'Action@edit_done_modal')->name('actionEditDoneModal');

    Route::get('/action/update/process/{id_action}', 'Action@update_process')->name('actionProcessUpdate');
    Route::post('/action/update/process/{id_action}', 'Action@update_process')->name('actionProcessUpdate');
    Route::post('/action/update/done/{id_action}', 'Action@update_done')->name('actionDoneUpdate');

    Route::get('/action/detail/{id_action}', 'Action@detail')->name('actionDetail');

});

Route::group(['namespace' => 'Control', 'middleware' => 'authLogin'], function () {

    Route::get('/control', 'Control@index')->name('controlList');
    Route::post('/conntrol/datatable', 'Control@data_table')->name('controlDataTable');
    Route::get('/control/done/modal/{id_control}', 'Control@done_modal')->name('controlDoneModal');
    Route::post('/control/done/process/{id_control}', 'Control@done_process')->name('controlDoneProcess');
    Route::get('/control/cancel/process/{id_control}', 'Control@cancel_process')->name('controlCancelProcess');
    Route::get('/control/edit/modal/{id_control}', 'Control@edit_modal')->name('controlEditModal');
    Route::post('/control/update/{id_control}', 'Control@update')->name('controlUpdate');
    Route::get('/control/detail/{id_control}', 'Control@detail')->name('controlDetail');

});

Route::group(['namespace' => 'Done', 'middleware' => 'authLogin'], function () {

    Route::get('/done', 'Done@index')->name('doneList');
    Route::get('/done/export/excel', 'Done@export_excel')->name('doneExportExcel');
    Route::post('/done/datatable', 'Done@data_table')->name('doneDataTable');
    Route::get('/done/back/{id_done}', 'Done@back_process')->name('doneBackProcess');
    Route::get('/done/detail/{id_done}', 'Done@detail')->name('doneDetail');

});

Route::group(['namespace' => 'Payment', 'middleware' => 'authLogin'], function () {

    Route::get('/payment', 'Payment@index')->name('paymentList');
    Route::get('/payment/export/excel', 'Payment@export_excel')->name('paymentExportExcel');
    Route::post('/payment/datatable', 'Payment@data_table')->name('paymentDataTable');
    Route::get('/payment/edit/{id_payment}', 'Payment@edit')->name('paymentEdit');
    Route::post('/payment/update/{id_payment}', 'Payment@update')->name('paymentUpdate');
    Route::get('/payment/print/{id_payment}/{type?}', 'Payment@print')->name('paymentPrint');

});

Route::group(['namespace' => 'Patient', 'middleware' => 'authLogin'], function () {

    Route::get('/patient', 'Patient@index')->name('patientList');
    Route::get('/patient/export/excel', 'Patient@export_excel')->name('patientExportExcel');
    Route::post('/patient/datatable', 'Patient@data_table')->name('patientDataTable');
    Route::get('/patient/medic-record/print/{id_patient}', 'Patient@medic_record_print')->name('patientMedicRecordPrint');
    Route::get('/patient/medic-record/edit/{id_patient}', 'Patient@medic_record_edit')->name('patientMedicRecordEdit');
    Route::post('/patient/medic-record/update/{id_patient}', 'Patient@medic_record_update')->name('patientMedicRecordUpdate');

});

Route::group(['namespace' => 'ReminderSetting', 'middleware' => 'authLogin'], function () {

    Route::get('/reminder/setting', 'ReminderSetting@index')->name('reminderSettingList');
    Route::post('/reminder/setting/update', 'ReminderSetting@update')->name('reminderSettingUpdate');

});

Route::group(['namespace' => 'ReminderHistory', 'middleware' => 'authLogin'], function () {

    Route::get('/reminder/history', 'ReminderHistory@index')->name('reminderHistoryList');
    Route::post('/reminder/history/datatable', 'ReminderHistory@data_table')->name('reminderHistoryDataTable');
    Route::get('/reminder/history/detail/{id_reminder_history}', 'ReminderHistory@detail_modal')->name('reminderHistoryDetail');

});

Route::group(['namespace' => 'ReminderMessage', 'middleware' => 'authLogin'], function () {

    Route::get('/reminder/message', "ReminderMessage@index")->name('reminderMessageList');
    Route::post('/reminder/message', "ReminderMessage@insert")->name('reminderMessageInsert');
    Route::get('/reminder/message/{id_reminder_message}', "ReminderMessage@edit_modal")->name('reminderMessageEditModal');
    Route::post('/reminder/message/{id_reminder_message}', "ReminderMessage@update")->name('reminderMessageUpdate');

});

Route::group(['namespace' => 'WhatsAppLink', 'middleware' => 'authLogin'], function () {

    Route::get('/whatsapp/link', 'WhatsAppLink@index')->name('whatsAppLink');

});

Route::group(['namespace' => 'ScheduleCalendar', 'middleware' => 'authLogin'], function () {

    Route::get('/schedule/calendar', 'ScheduleCalendar@index')->name('scheduleCalendarPage');
    Route::get('/schedule/modal', 'ScheduleCalendar@modal')->name('scheduleCalendarModal');

});

Route::group(['namespace' => 'ScheduleCalendar'], function () {
    Route::get('/schedule/doctor', 'ScheduleCalendar@doctor')->name('scheduleDoctorPage');

});

Route::namespace('General')->group(function () {

    Route::get('patient/done/total', 'General@patient_done_total');

    Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');
    Route::post('city/list', "General@city_list")->name('cityList');
    Route::post('subdistrict/list', "General@subdistrict_list")->name('subdistrictList');

    Route::group(['middleware' => 'checkLogin'], function () {
        Route::get('/', "Login@index")->name("loginPage");
        Route::post('/roles/login', 'Login@roles')->name("rolesLogin");
        Route::post('/login', "Login@do")->name("loginDo");
    });

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/dashboard', "Dashboard@index")->name('dashboardPage');
        Route::get('/whatsapp-test', "Dashboard@whatsapp_test")->name('whatsappTest');
        Route::get('/logout', "Logout@do")->name('logoutDo');
        Route::get('/profile', "Profile@index")->name('profilPage');
        Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');

    });
});

Route::group(['namespace'=>'General'], function() {
    Route::get('/reminder/run_mwz', 'Reminder@run');
    Route::get('/reminder/consult_mwz', 'Reminder@consult');
    Route::get('/reminder/action_mwz', 'Reminder@action');
    Route::get('/reminder/control_mwz', 'Reminder@control');
    Route::get('/reminder/followup_short_mwz', 'Reminder@followup_short');
    Route::get('/reminder/followup_long_mwz', 'Reminder@followup_long');
    Route::get('/reminder/birthday_mwz', 'Reminder@birthday');
    Route::get('/reminder/schedule_mwz', 'Reminder@schedule');
    Route::get('/reminder/schedule_2_mwz', 'Reminder@schedule_2');
    Route::get('/reminder/birthday_mwz', 'Reminder@birthday');
    Route::get('/reminder/test_mwz', 'Reminder@test_cron_job');

//    Route::get('/reminder/test_message', 'Reminder@test_message');
});

// =============== Master Data =================== //

Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin'], function () {

    // Karyawan
    Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
    Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
    Route::get('/karyawan-list', "Karyawan@list")->name('karyawanList');
    Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
    Route::get('/karyawan/{id}', "Karyawan@edit_modal")->name('karyawanEditModal');
    Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');

    // User
    Route::get('/user', "User@index")->name('userPage');
    Route::post('/user', "User@insert")->name('userInsert');
    Route::get('/user-list', "User@list")->name('userList');
    Route::delete('/user/{kode}', "User@delete")->name('userDelete');
    Route::get('/user/modal/{kode}', "User@edit_modal")->name('userEditModal');
    Route::post('/user/{kode}', "User@update")->name('userUpdate');

    // Role User
    Route::get('/user-role', "UserRole@index")->name('userRolePage');
    Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
    Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
    Route::get('/user-role/modal/{id}', "UserRole@edit")->name('userRoleEditModal');
    Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
    Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
    Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

    // Metode Tindakan
    Route::get('/metode-tindakan', "ActionMethod@index")->name('actionMethodPage');
    Route::post('/metode-tindakan', "ActionMethod@insert")->name('actionMethodInsert');
    Route::get('/metode-tindakan-list', "ActionMethod@list")->name('actionMethodList');
    Route::delete('/metode-tindakan/{kode}', "ActionMethod@delete")->name('actionMethodDelete');
    Route::get('/metode-tindakan/modal/{kode}', "ActionMethod@edit_modal")->name('actionMethodEditModal');
    Route::post('/metode-tindakan/{kode}', "ActionMethod@update")->name('actionMethodUpdate');

    // Distrik
    Route::get('/distrik', "District@index")->name('districtPage');
    Route::post('/distrik', "District@insert")->name('districtInsert');
    Route::get('/distrik-list', "District@list")->name('districtList');
    Route::delete('/distrik/{kode}', "District@delete")->name('districtDelete');
    Route::get('/distrik/modal/{kode}', "District@edit_modal")->name('districtEditModal');
    Route::post('/distrik/{kode}', "District@update")->name('districtUpdate');

    // Sub Distrik
    Route::get('/subdistrik', "Subdistrict@index")->name('subdistrictPage');
    Route::post('/subdistrik', "Subdistrict@insert")->name('subdistrictInsert');
    Route::get('/subdistrik-list', "Subdistrict@list")->name('subdistrictList');
    Route::delete('/subdistrik/{kode}', "Subdistrict@delete")->name('subdistrictDelete');
    Route::get('/subdistrik/modal/{kode}', "Subdistrict@edit_modal")->name('subdistrictEditModal');
    Route::post('/subdistrik/{kode}', "Subdistrict@update")->name('subdistrictUpdate');

});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

