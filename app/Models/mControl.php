<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mControl extends Model
{
    use SoftDeletes;

    protected $table = 'control';
    protected $primaryKey = 'id_control';
    protected $fillable = [
        'id_patient',
        'id_appointment',
        'id_action',
        'id_user',
        'cancel_time',
        'control_location',
        'control_time',
        'control_done_time',
        'control_back_time',
        'control_number',
        'control_done_notes',
        'status',
    ];

    function action() {
        return $this->belongsTo(mAction::class, 'id_action');
    }

    function patient() {
        return $this->belongsTo(mPatient::class, 'id_patient');
    }

    function consult() {
        return $this->belongsTo(mConsult::class, 'id_consult');
    }

    function appointment() {
        return $this->belongsTo(mAppointment::class, 'id_appointment');
    }

    function control_list() {
        return $this->hasMany(mControl::class, 'id_action', 'id_action');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
