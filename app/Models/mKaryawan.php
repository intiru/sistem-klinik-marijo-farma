<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mKaryawan extends Model
{
    use SoftDeletes;

    protected $table = 'tb_karyawan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_karyawan',
        'nama_karyawan',
        'alamat_karyawan',
        'telp_karyawan',
        'posisi_karyawan',
        'email_karyawan',
        'foto_karyawan',
        'poli',
        'jadwal'
    ];

    public function user()
    {
        return $this->hasMany(mUser::class, 'id');
    }

    public function count_all()
    {
        return DB::table($this->table)->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table);
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
