<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPayment extends Model
{
    use SoftDeletes;

    protected $table = 'payment';
    protected $primaryKey = 'id_payment';
    protected $fillable = [
        'id_action',
        'id_patient',
        'id_user',
        'invoice_number',
        'invoice_label',
        'invoice_date',
        'total',
        'additional_cost',
        'discount',
        'grand_total'
    ];

    function payment_detail() {
        return $this->hasMany(mPaymentDetail::class, 'id_payment');
    }

    function patient() {
        return $this->belongsTo(mPatient::class, 'id_patient');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
