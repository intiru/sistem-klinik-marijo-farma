<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPatient extends Model
{
    use SoftDeletes;

    protected $table = 'patient';
    protected $primaryKey = 'id_patient';
    protected $fillable = [
        'id_province',
        'id_city',
        'id_subdistrict',
        'id_user',
        'id_karyawan_doctor',
        'id_karyawan_nurse',
        'name',
        'name_responsible',
        'name_responsible_relation',
        'birth_place',
        'birthday',
        'weight',
        'temperature',
        'complaint',
        'medic_record_number',
        'district',
        'subdistrict',
        'poli',
        'address',
        'phone_1',
        'phone_2',
        'status'
    ];

    function city() {
        return $this->belongsTo(mCity::class, 'id_city', 'id_city');
    }

    function subdistrict() {
        return $this->belongsTo(mSubdistrict::class, 'id_subdistrict', 'id_subdistrict');
    }

    function province() {
        return $this->belongsTo(mProvince::class, 'id_province', 'id_province');
    }

    function medic_record() {
        return $this->belongsTo(mMedicRecord::class, 'id_patient', 'id_patient');
    }

    function doctor() {
        return $this->belongsTo(mKaryawan::class, 'id_karyawan_doctor', 'id');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
