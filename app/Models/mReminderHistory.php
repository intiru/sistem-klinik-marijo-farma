<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mReminderHistory extends Model
{
    use SoftDeletes;

    protected $table = 'reminder_history';
    protected $primaryKey = 'id_reminder_history';
    protected $fillable = [
        'id_appointment',
        'id_action',
        'id_patient',
        'status',
        'type',
        'title',
        'message',
        'phone',
        'email',
        'response',
        'success',
        'description',
        'result_code'
    ];

    public function appointment() {
        return $this->belongsTo(mAppointment::class, 'id_appointment', 'id_appointment');
    }

    public function action() {
        return $this->belongsTo(mAction::class, 'id_action', 'id_action');
    }

    public function patient() {
        return $this->belongsTo(mPatient::class, 'id_patient', 'id_patient');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
