<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mReminderSetting extends Model
{
    use SoftDeletes;

    protected $table = 'reminder_setting';
    protected $primaryKey = 'id_reminder_setting';
    protected $fillable = [
        'label',
        'status',
        'target_day',
        'target_hour',
        'variable'
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
