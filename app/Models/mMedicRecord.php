<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mMedicRecord extends Model
{
    use SoftDeletes;

    protected $table = 'medic_record';
    protected $primaryKey = 'id_medic_record';
    protected $fillable = [
        'id_patient',
        'id_action',
        'id_user',
        'disease_now',
        'disease_before',
        'medical_history',
        'alergy_history',
        'sign_tension',
        'sign_temp',
        'sign_pulse',
        'sign_rr',
        'general_condition',
        'genital_check',
        'assessment',
        'planning',
        'diagnosa'
    ];

    function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
