<?php

namespace app\Http\Controllers\Consult;

use app\Models\mAction;
use app\Models\mAppointment;
use app\Models\mConsult;
use app\Models\mControl;
use app\Models\mPatient;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;
use Illuminate\Support\Facades\Session;

class Consult extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['consult'],
                'route' => ''
            ],
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $name = $request->name;
        $date_from = $request->date_from ? $request->date_from : date('01-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));
        $datatable_column = [
            ["data" => "no"],
            ["data" => "name"],
            ["data" => "weight"],
            ["data" => "age"],
            ["data" => "consult_location"],
            ["data" => "consult_time"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'name' => $name,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'datatable_column' => $datatable_column
        ));

        return view('consult/consult/consultList', $data);
    }


    function data_table(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $name = $request->name;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $total_data = mConsult
            ::leftJoin('patient', 'patient.id_patient', '=', 'consult.id_patient')
            ->whereIn('consult.status', ['process'])
            ->whereBetween('consult.consult_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_consult'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mConsult
            ::leftJoin('patient', 'patient.id_patient', '=', 'consult.id_patient')
            ->whereIn('consult.status', ['process'])
            ->whereBetween('consult.consult_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_consult = Main::encrypt($row->id_consult);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['name'] = $row->name;
            $nestedData['weight'] = $row->weight . ' Kg';
            $nestedData['age'] = Main::format_age($row->birthday);
            $nestedData['consult_location'] = ucwords($row->consult_location);
            $nestedData['consult_time'] = Main::format_datetime($row->consult_time);
            $nestedData['options'] = '
                
                    <div class="dropdown">
                        <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            Menu
                        </button>
                        <div class="dropdown-menu dropdown-menu-right"
                             aria-labelledby="dropdownMenuButton">
                            <a class="akses-consult_done dropdown-item btn-modal-general"
                               data-route="' . route('consultDoneModal', ['id_consult' => $id_consult]) . '"
                               href="#">
                                <i class="la la-check"></i>
                                Konsultasi Selesai
                            </a>
                            <a class="akses-consult_cancel dropdown-item btn-modal-general"
                               href="#"
                                data-route="' . route('consultCancelModal', ['id_consult' => $id_consult]) . '">
                                <i class="la la-close"></i>
                                Konsultasi Batal
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="akses-edit dropdown-item btn-modal-general"
                               href="#"
                               data-route="' . route('consultEdit', ['id_consult' => $id_consult]) . '">
                                <i class="la la-pencil"></i>
                                Edit
                            </a>
                            <a class="akses-detail dropdown-item btn-modal-general"
                               href="#"
                                data-route="' . route('consultDetail', ['id_consult' => $id_consult]) . '">
                                <i class="la la-info"></i>
                                Detail
                            </a>
                        </div>
                    </div>
                ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function detail($id_consult)
    {
        $id_consult = Main::decrypt($id_consult);
        $row = mConsult
            ::with([
                'patient',
                'patient.province',
                'patient.city',
                'patient.subdistrict',
                'appointment'
            ])
            ->where('id_consult', $id_consult)
            ->first();
        $data = [
            'row' => $row
        ];


        return view('consult/consult/consultDetail', $data);
    }

    function edit($id_consult)
    {
        $id_consult = Main::decrypt($id_consult);
        $edit = mConsult::where('id_consult', $id_consult)->first();

        $data = [
            'edit' => $edit
        ];

        return view('consult/consult/consultEdit', $data);
    }

    function update(Request $request, $id_consult)
    {
        $request->validate([
            'consult_location' => 'required',
            'consult_date' => 'required',
            'consult_hours' => 'required'
        ]);

        $id_consult = Main::decrypt($id_consult);
        $consult_location = $request->consult_location;
        $consult_date = $request->consult_date;
        $consult_hours = $request->consult_hours;
        $consult_time = Main::format_date_db($consult_date) . ' ' . Main::format_time_db($consult_hours);
        $id_appointment = mConsult::where('id_consult', $id_consult)->value('id_appointment');

        mConsult
            ::where([
                'id_consult' => $id_consult
            ])
            ->update([
                'consult_location' => $consult_location,
                'consult_time' => $consult_time
            ]);

        mAppointment
            ::where([
                'id_appointment' => $id_appointment
            ])
            ->update([
                'appointment_time' => $consult_time
            ]);


    }

    function done_modal($id_consult)
    {
        $id_consult = Main::decrypt($id_consult);
        $row = mConsult
            ::where('id_consult', $id_consult)
            ->first();
        $data = [
            'row' => $row
        ];

        return view('consult/consult/consultDoneModal', $data);
    }

    function done_process(Request $request, $id_consult)
    {
        $request->validate([
            'action_date' => 'required',
            'action_hours' => 'required',
            'whatsapp_send' => 'required',
//            'consult_done_notes' => 'required',
        ]);

        $id_consult = Main::decrypt($id_consult);
        $consult = mConsult::with('appointment')->where('id_consult', $id_consult)->first();
        $action_date = $request->action_date;
        $action_hours = $request->action_hours;
        $whatsapp_send = $request->whatsapp_send;
        $action_time = Main::format_date_db($action_date) . ' ' . Main::format_time_db($action_hours);
        $consult_done_notes = $request->consult_done_notes;
        $id_user = Session::get('user')['id'];


        DB::beginTransaction();
        try {

            mConsult
                ::where([
                    'id_consult' => $id_consult
                ])
                ->update([
                    'done_time' => date('Y-m-d H:i:s'),
                    'status' => 'done',
                    'consult_done_notes' => $consult_done_notes
                ]);
            mAppointment::where('id_appointment', $consult->id_appointment)->update(['status' => 'done']);

            $data_action = [
                'id_patient' => $consult->id_patient,
                'id_consult' => $id_consult,
                'id_user' => $id_user,
                'action_location' => $consult->consult_location,
                'action_time' => $action_time,
                'status' => 'process',
                'whatsapp_status_short' => 'not_yet',
                'whatsapp_status_long' => 'not_yet',
            ];
            $id_action = mAction::create($data_action)->id_action;

            $data_appointment = [
                'id_consult' => $id_consult,
                'id_action' => $id_action,
                'id_patient' => $consult->id_patient,
                'id_user' => $id_user,
                'need_type' => 'action',
                'appointment_time' => $action_time,
                'via' => 'system',
                'status' => 'prepare',
                'whatsapp_send' => $whatsapp_send
            ];

            $id_appointment = mAppointment::create($data_appointment)->id_appointment;
            mAction::where('id_action', $id_action)->update(['id_appointment' => $id_appointment]);

            mPatient::where('id_patient', $consult->id_patient)->update(['status' => 'consult']);

            DB::commit();

        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }


    }

    function cancel_modal($id_consult)
    {
        $id_consult = Main::decrypt($id_consult);
        $row = mConsult
            ::where('id_consult', $id_consult)
            ->first();
        $data = [
            'row' => $row
        ];

        return view('consult/consult/consultCancelModal', $data);
    }

    function cancel_process(Request $request, $id_consult)
    {
        $request->validate([
            'consult_cancel_notes' => 'required',
        ]);

        $id_consult = Main::decrypt($id_consult);
        $id_patient = mConsult::where('id_consult', $id_consult)->value('id_patient');
        $consult_cancel_notes = $request->consult_cancel_notes;
        $id_appointment = mConsult::where('id_consult', $id_consult)->value('id_appointment');


        DB::beginTransaction();
        try {
            mConsult
                ::where([
                    'id_consult' => $id_consult
                ])
                ->update([
                    'cancel_time' => date('Y-m-d H:i:s'),
                    'status' => 'cancel',
                    'consult_cancel_notes' => $consult_cancel_notes
                ]);

            mAppointment::where('id_appointment', $id_appointment)->update(['status' => 'cancel']);

            mPatient::where('id_patient', $id_patient)->update(['status' => 'cancel']);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

}
