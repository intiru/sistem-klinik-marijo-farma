<?php

namespace app\Http\Controllers\ReminderMessage;

use app\Models\mReminderMessage;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Hash;
use app\Rules\UsernameChecker;
use app\Rules\UsernameCheckerUpdate;
use Illuminate\Support\Facades\Config;

use app\Models\mKaryawan;
use app\Models\mUser;
use app\Models\mUserRole;

class ReminderMessage extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['reminder_message'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $reminder = mReminderMessage
            ::orderBy('title', 'ASC')
            ->get();

        $data = array_merge($data, [
            'reminder' => $reminder,
        ]);

        return view('reminderMessage/reminderMessage/reminderMessageList', $data);
    }

    function edit_modal($id_reminder_message)
    {
        $id_reminder_message = Main::decrypt($id_reminder_message);
        $edit = mReminderMessage::where('id_reminder_message', $id_reminder_message)->first();


        $variable = [
            '{salam_pembuka}' => 'Salam Pembukan (ex: Selamat Pagi)',
            '{jam_appointment}' => 'Jam Appointment',
            '{hari_appointment}' => 'Hari Appointment',
            '{tanggal_appointment}' => 'Tanggal Appointment',

            '{jumlah_follow_up_jangka_pendek}' => 'Jumlah Follow Up Jangka Pendek',
            '{jumlah_follow_up_jangka_panjang}' => 'Jumlah Follow Up Jangka Panjang',

            '{nama_pasien}' => 'Nama Pasien',
            '{tanggal_ulang_tahun_pasien}' => 'Tanggal Ulang Tahun Pasien',
            '{umur_pasien}' => 'Umur Pasien',
            '{berat_pasien}' => 'Berat Badan Pasien',
            '{tahap_kontrol}' => 'Tahap kontrol',
        ];

        $data = [
            'edit' => $edit,
            'variable_list' => $variable,
            'no' => 1
        ];

        return view('reminderMessage/reminderMessage/reminderMessageEditModal', $data);
    }

    function update(Request $request, $id_reminder_message)
    {
        $id_reminder_message = Main::decrypt($id_reminder_message);
        $request->validate([
            'message' => 'required'
        ]);
        $data = $request->except("_token");
        mReminderMessage::where(['id_reminder_message' => $id_reminder_message])->update($data);
    }
}
