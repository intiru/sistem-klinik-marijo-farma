<?php

namespace app\Http\Controllers\Action;

use app\Models\mAction;
use app\Models\mActionMethod;
use app\Models\mAppointment;
use app\Models\mConsult;
use app\Models\mControl;
use app\Models\mMedicRecord;
use app\Models\mPatient;
use app\Models\mPayment;
use app\Models\mPaymentDetail;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;
use Illuminate\Support\Facades\Session;

class Action extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['action'];
        $this->breadcrumb = [
            [
                'label' => 'Daftar Tindakan',
                'route' => route('actionProcessList')
            ],
        ];
    }

    function index_process(Request $request)
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);

        $name = $request->name;
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y");
        $datatable_column = [
            ["data" => "no"],
            ["data" => "name"],
            ["data" => "weight"],
            ["data" => "age"],
//            ["data" => "action_location"],
            ["data" => "action_time"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'name' => $name,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'datatable_column' => $datatable_column,
        ));

        return view('action/action/actionProcessList', $data);
    }

    function index_done(Request $request)
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);

        $name = $request->name;
        $date_from = date('d-m-Y'); //$request->date_from ? $request->date_from : date('01-m-Y');
        $date_to = date('d-m-Y'); //$request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));
        $datatable_column = [
            ["data" => "no"],
            ["data" => "name"],
            ["data" => "weight"],
            ["data" => "age"],
//            ["data" => "action_location"],
            ["data" => "action_time"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'name' => $name,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'datatable_column' => $datatable_column,
        ));

        return view('action/action/actionDoneList', $data);
    }


    function process_data_table(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $name = $request->name;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $total_data = mAction
            ::leftJoin('patient', 'patient.id_patient', '=', 'action.id_patient')
            ->whereIn('action.status', ['process'])
            ->whereBetween('action.action_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_action'; //$columns[$request->input('order.0.column')];
        $order_type = 'asc';//$request->input('order.0.dir');

        $data_list = mAction
            ::leftJoin('patient', 'patient.id_patient', '=', 'action.id_patient')
            ->whereIn('action.status', ['process'])
            ->whereBetween('action.action_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_action = Main::encrypt($row->id_action);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['name'] = $row->name;
            $nestedData['weight'] = $row->weight . ' Kg';
            $nestedData['age'] = Main::format_age($row->birthday);
            $nestedData['action_location'] = ucwords($row->action_location);
            $nestedData['action_time'] = Main::format_datetime($row->action_time);
            $nestedData['options'] = '
                
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done dropdown-item"
                           href="' . route('actionDonePage', ['id_action' => $id_action]) . '">
                            <i class="la la-check"></i>
                            Tindakan Selesai
                        </a>';

            if ($row->id_consult > 0) {
                $nestedData['options'] .= '<a class="akses-action_wait_back dropdown-item"
                                       href = "' . route('actionBack', ['id_action' => $id_action, 'date_from' => $date_from, 'date_to' => $date_to, 'name' => $name]) . '" >
                                        <i class="la la-backward" ></i >
                                    Kembalikan ke Konsultasi
                                    </a >';
            }

            $nestedData['options'] .= '

                        <a class="akses-action_wait_cancel dropdown-item"
                           href = "' . route('actionCancel', ['id_action' => $id_action, 'date_from' => $date_from, 'date_to' => $date_to, 'name' => $name]) . '" >
                            <i class="la la-remove" ></i >
                        Tindakan Batal
                        </a >

                        <div class="dropdown-divider"></div>
                        <a class="akses-action_wait_edit dropdown-item btn-modal-general"
                           data-route="' . route('actionEditProcessModal', ['id_action' => $id_action]) . '"
                           href="#">
                            <i class="la la-pencil"></i>
                            Edit
                        </a>
                        <a class="akses-action_wait_detail dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('actionDetail', ['id_action' => $id_action]) . '">
                            <i class="la la-info"></i>
                            Detail
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function done_data_table(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $name = $request->name;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $total_data = mAction
            ::leftJoin('patient', 'patient.id_patient', '=', 'action.id_patient')
            ->whereIn('action.status', ['done'])
            ->whereBetween('action.action_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_action'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mAction
            ::leftJoin('patient', 'patient.id_patient', '=', 'action.id_patient')
            ->whereIn('action.status', ['done'])
            ->whereBetween('action.action_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_action = Main::encrypt($row->id_action);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['name'] = $row->name;
            $nestedData['weight'] = $row->weight . ' Kg';
            $nestedData['age'] = Main::format_age($row->birthday);
            $nestedData['action_location'] = ucwords($row->action_location);
            $nestedData['action_time'] = Main::format_datetime($row->action_time);
            $nestedData['options'] = '
                
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_done_edit dropdown-item btn-modal-general m--hide"
                           data-route="' . route('actionEditDoneModal', ['id_action' => $id_action]) . '"
                           href="#">
                            <i class="la la-pencil"></i>
                            Edit
                        </a>
                        <a class="akses-action_done_detail dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('actionDetail', ['id_action' => $id_action]) . '">
                            <i class="la la-info"></i>
                            Detail
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function done_page($id_action)
    {

        $breadcrumb = array_merge(
            $this->breadcrumb, array(
                array(
                    'label' => 'Tindakan Selesai',
                    'route' => ''
                )
            )
        );

        $data = Main::data($breadcrumb, $this->menuActive);
//        $schedule_calendar_modal = Main::scheduleCalendarModal();
        $action_method = mActionMethod::orderBy('title','ASC')->get();
        $id_action = Main::decrypt($id_action);
        $action = mAction
            ::with([
                'patient',
                'patient.province',
                'patient.subdistrict',
                'patient.city',
                'patient.medic_record',
            ])
            ->where('id_action', $id_action)
            ->first();

        $control_date = date('Y-m-d', strtotime($action->action_time . ' + 4 days'));

        $data = array_merge($data, array(
//            'schedule_calendar_modal' => $schedule_calendar_modal,
            'action' => $action,
            'control_date' => $control_date,
            'action_method' => $action_method
        ));

        return view('action/action/actionDone', $data);
    }

    function done_process(Request $request, $id_action)
    {
        $request->validate([
            'id_action_method' => 'required',
            'control_date' => 'required',
            'control_hours' => 'required',
            'disease_now' => 'required',
            'disease_before' => 'required',
            'medical_history' => 'required',
            'alergy_history' => 'required',
            'sign_tension' => 'required',
            'sign_temp' => 'required',
            'sign_pulse' => 'required',
            'sign_rr' => 'required',
            'general_condition' => 'required',
            'genital_check' => 'required',
            'assessment' => 'required',
            'planning' => 'required',
            'description.*' => 'required',
            'diagnosa.*' => 'required',
//            'qty.*' => 'required',
//            'price.*' => 'required',
//            'payment_plus' => 'required',
//            'payment_cut' => 'required',
        ]);

        $id_action = Main::decrypt($id_action);

        $id_action_method = $request->id_action_method;
        $rawat = $request->rawat;
        $lanjut_kontrol = $request->lanjut_kontrol;
        $control_date = $request->control_date;
        $control_hours = $request->control_hours;
        $control_time = Main::format_date_db($control_date) . ' ' . Main::format_time_db($control_hours);
        $action_done_notes = $request->action_done_notes;
        $disease_now = $request->disease_now;
        $disease_before = $request->disease_before;
        $medical_history = $request->medical_history;
        $alergy_history = $request->alergy_history;
        $sign_tension = $request->sign_tension;
        $sign_temp = $request->sign_temp;
        $sign_pulse = $request->sign_pulse;
        $sign_rr = $request->sign_rr;
        $general_condition = $request->general_condition;
        $genital_check = $request->genital_check;
        $assessment = $request->assessment;
        $planning = $request->planning;
        $diagnosa = $request->diagnosa;
        $description_arr = $request->description;
        $qty_arr = $request->qty;
        $price_arr = $request->price;
        $payment_plus = $request->payment_plus;
        $payment_cut = $request->payment_cut;
        $id_user = Session::get('user')['id'];
        $invoice_number = Main::invoiceNumber();
        $invoice_label = Main::invoiceLabel($invoice_number);
        $invoice_date = date('Y-m-d H:i:s');
        $total = $request->total;
        $grand_total = $request->grand_total;
        $action = mAction::where('id_action', $id_action)->first();

        DB::beginTransaction();
        try {

            /**
             * Update Action & Appointment
             */
            $data_action = [
                'action_done_notes' => $action_done_notes,
                'done_time' => date('Y-m-d H:i:s'),
                'status' => 'done',
                'id_action_method' => $id_action_method,
                'rawat' => $rawat,
                'lanjut_kontrol' => $lanjut_kontrol,
            ];
            mAction::where('id_action', $id_action)->update($data_action);

            mAppointment::where('id_action', $id_action)->update(['status' => 'done']);

            /**
             * Create Next Data
             */
            $data_control = [
                'id_patient' => $action->id_patient,
                'id_action' => $id_action,
                'id_user' => $id_user,
                'control_location' => $action->action_location,
                'control_time' => $control_time,
                'control_number' => 1,
                'status' => 'process'
            ];
            $id_control = mControl::create($data_control)->id_control;

            $data_appointment = [
                'id_control' => $id_control,
                'id_patient' => $action->id_patient,
                'id_user' => $id_user,
                'need_type' => 'control',
                'control_step' => 1,
                'appointment_time' => $control_time,
                'via' => 'system',
                'status' => 'prepare'
            ];
            $id_appointment = mAppointment::create($data_appointment)->id_appointment;
            mControl::where('id_control', $id_control)->update(['id_appointment' => $id_appointment]);

            /**
             * Create or Update Rekam Medis Pasien
             */
            $check_medic_exist = mMedicRecord::where('id_patient', $action->id_patient)->count();
            if ($check_medic_exist == 0) {
                $data_medic = [
                    'id_patient' => $action->id_patient,
                    'id_action' => $id_action,
                    'id_user' => $id_user,
                    'disease_now' => $disease_now,
                    'disease_before' => $disease_before,
                    'medical_history' => $medical_history,
                    'alergy_history' => $alergy_history,
                    'sign_tension' => $sign_tension,
                    'sign_temp' => $sign_temp,
                    'sign_pulse' => $sign_pulse,
                    'sign_rr' => $sign_rr,
                    'general_condition' => $general_condition,
                    'genital_check' => $genital_check,
                    'assessment' => $assessment,
                    'planning' => $planning,
                    'diagnosa' => $diagnosa
                ];
                mMedicRecord::create($data_medic);
            } else {
                $data_medic = [
                    'id_action' => $id_action,
                    'id_user' => $id_user,
                    'disease_now' => $disease_now,
                    'disease_before' => $disease_before,
                    'medical_history' => $medical_history,
                    'alergy_history' => $alergy_history,
                    'sign_tension' => $sign_tension,
                    'sign_temp' => $sign_temp,
                    'sign_pulse' => $sign_pulse,
                    'sign_rr' => $sign_rr,
                    'general_condition' => $general_condition,
                    'genital_check' => $genital_check,
                    'assessment' => $assessment,
                    'planning' => $planning,
                    'diagnosa' => $diagnosa
                ];
                mMedicRecord::where('id_patient', $action->id_patient)->update($data_medic);
            }

            /**
             * Create Payment
             */
            $data_payment = [
                'id_action' => $id_action,
                'id_patient' => $action->id_patient,
                'id_user' => $id_user,
                'invoice_number' => $invoice_number,
                'invoice_label' => $invoice_label,
                'invoice_date' => $invoice_date,
                'total' => $total,
                'additional_cost' => $payment_plus,
                'discount' => $payment_cut,
                'grand_total' => $grand_total
            ];

            $id_payment = mPayment::create($data_payment)->id_payment;

            foreach ($description_arr as $key => $description) {
                $qty = Main::format_number_db($qty_arr[$key]);
                $price = Main::format_number_db($price_arr[$key]);
                $total = $qty * $price;

                $data_payment_detail = [
                    'id_payment' => $id_payment,
                    'id_action' => $id_action,
                    'id_patient' => $action->id_patient,
                    'id_user' => $id_user,
                    'description' => $description,
                    'qty' => $qty,
                    'price' => $price,
                    'total' => $total
                ];

                mPaymentDetail::create($data_payment_detail);
            }

            mPatient
                ::where('id_patient', $action->id_patient)
                ->update([
                    'status' => 'action'
                ]);

            DB::commit();

        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

        $session = [
            'download_pdf' => 'no',
//            'id_payment' => $id_payment,
            'id_patient' => $action->id_patient
        ];
        Session::put($session);
    }

    function back_process(Request $request, $id_action)
    {
        $id_action = Main::decrypt($id_action);
        $name = $request->name;
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date('d-m-Y');
        $action = mAction::where('id_action', $id_action)->first();
        $data_update = [
            'back_time' => date('Y-m-d H:i:s'),
            'status' => 'back'
        ];

        DB::beginTransaction();
        try {
            mAction::where('id_action', $id_action)->update($data_update);
            mAppointment::where('id_appointment', $action->id_appointment)->update(['status' => 'cancel']);

            mConsult
                ::where('id_consult', $action->id_consult)
                ->update([
                    'status' => 'process'
                ]);
            mPatient::where('id_patient', $action->id_patient)->update(['status' => 'consult']);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
        return redirect()->route('actionProcessList', ['date_from' => $date_from, 'date_to' => $date_to, 'name' => $name]);

    }

    function cancel_process(Request $request, $id_action)
    {
        $id_action = Main::decrypt($id_action);
        $name = $request->name;
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date('d-m-Y');
        $action = mAction::where('id_action', $id_action)->first();
        $data_update = [
            'cancel_time' => date('Y-m-d H:i:s'),
            'status' => 'cancel'
        ];

        DB::beginTransaction();
        try {
            mAction::where('id_action', $id_action)->update($data_update);
            mAppointment::where('id_appointment', $action->id_appointment)->update(['status' => 'cancel']);
            mPatient::where('id_patient', $action->id_patient)->update(['status' => 'cancel']);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
        return redirect()->route('actionProcessList', ['date_from' => $date_from, 'date_to' => $date_to, 'name' => $name]);

    }

    function edit_process_modal($id_action)
    {
        $id_action = Main::decrypt($id_action);
        $action = mAction::where('id_action', $id_action)->first();

        $data = [
            'edit' => $action,
        ];

        return view('action/action/actionEditProcess', $data);
    }

    function edit_done_modal($id_action)
    {
        $id_action = Main::decrypt($id_action);
        $action = mAction::where('id_action', $id_action)->first();
        $action_method = mActionMethod::orderBy('title', 'ASC')->get();

        $data = [
            'edit' => $action,
            'action_method' => $action_method
        ];

        return view('action/action/actionEditDone', $data);
    }

    function update_process(Request $request, $id_action)
    {
        $request->validate([
            'action_location' => 'required',
            'action_date' => 'required',
            'action_hours' => 'required'
        ]);

        $id_action = Main::decrypt($id_action);
        $action_location = $request->action_location;
        $action_date = $request->action_date;
        $action_hours = $request->action_hours;
        $action_time = Main::format_date_db($action_date) . ' ' . Main::format_time_db($action_hours);
        $id_appointment = mAction::where('id_action', $id_action)->value('id_appointment');

        DB::beginTransaction();
        try {
            mAction
                ::where([
                    'id_action' => $id_action
                ])
                ->update([
                    'action_location' => $action_location,
                    'action_time' => $action_time
                ]);

            mAppointment
                ::where([
                    'id_appointment' => $id_appointment
                ])
                ->update([
                    'appointment_time' => $action_time
                ]);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function update_done(Request $request, $id_action)
    {
        $request->validate([
            'action_location' => 'required',
            'id_action_method' => 'required',
        ]);

        $id_action = Main::decrypt($id_action);
        $action_location = $request->action_location;
        $id_action_method = $request->id_action_method;

        DB::beginTransaction();
        try {
            mAction
                ::where([
                    'id_action' => $id_action
                ])
                ->update([
                    'action_location' => $action_location,
                    'id_action_method' => $id_action_method,
                ]);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function detail($id_action)
    {
        $id_action = Main::decrypt($id_action);
        $row = mAction
            ::with([
                'patient',
                'patient.province',
                'patient.city',
                'patient.subdistrict',
                'consult',
                'appointment'
            ])
            ->where('id_action', $id_action)
            ->first();

        $data = [
            'row' => $row
        ];

        return view('action/action/actionDetail', $data);
    }

}
