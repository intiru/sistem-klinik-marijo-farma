<?php

namespace app\Http\Controllers\ScheduleCalendar;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;

class ScheduleCalendar extends Controller
{
    private $breadcrumb;
    private $view = [
        'nama_karyawan' => ["search_field" => "tb_karyawan.nama_karyawan"],
        'posisi_karyawan' => ["search_field" => "tb_karyawan.posisi_karyawan"],
        'telp_karyawan' => ["search_field" => "tb_karyawan.telp_karyawan"],
        'alamat_karyawan' => ["search_field" => "tb_karyawan.alamat_karyawan"],
        'email_karyawan' => ["search_field" => "tb_karyawan.email_karyawan"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['schedule_calendar'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $schedule_calendar_modal = Main::scheduleCalendarModal();

        $data = array_merge($data, [
            'schedule_calendar_modal' => $schedule_calendar_modal
        ]);

        return view('scheduleCalendar/scheduleCalendar/scheduleCalendarList', $data);
    }

    function doctor()
    {
        $data = Main::data($this->breadcrumb);
        $doctor = mKaryawan::where('posisi_karyawan', 'Dokter')->orderBy('nama_karyawan', 'ASC')->get();

        $data = array_merge($data, [
            'dokter' => $doctor
        ]);

        return view('scheduleCalendar/scheduleCalendar/scheduleDoctorList', $data);
    }

}
