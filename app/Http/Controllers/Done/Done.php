<?php

namespace app\Http\Controllers\Done;

use app\Models\mAppointment;
use app\Models\mControl;
use app\Models\mDone;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;

class Done extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['done'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $name = $request->name;
        $date_from = $request->date_from ? $request->date_from : date('01-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));
        $datatable_column = [
            ["data" => "no"],
            ["data" => "name"],
            ["data" => "weight"],
            ["data" => "age"],
            ["data" => "done_time"],
            ["data" => "address"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'name' => $name,
            'date_from' => $date_from,
            'date_to' => $date_to
        ));

        return view('done/done/doneList', $data);
    }

    function data_table(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $name = $request->name;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $total_data = mDone
            ::leftJoin('patient', 'patient.id_patient', '=', 'done.id_patient')
            ->with([
                'patient:id_patient,id_city,id_province,id_subdistrict',
                'patient.city',
                'patient.province',
                'patient.subdistrict',
            ])
            ->whereIn('done.status', ['done'])
            ->whereBetween('done.done_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_done'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mDone
            ::leftJoin('patient', 'patient.id_patient', '=', 'done.id_patient')
            ->whereIn('done.status', ['done'])
            ->whereBetween('done.done_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_done = Main::encrypt($row->id_done);
            $id_patient = Main::encrypt($row->id_patient);
            $id_control = Main::encrypt($row->id_control);
            $address = $row->patient->province->province_name . ', ' . $row->patient->city->city_name . ', ' . $row->patient->subdistrict->subdistrict_name . ', ' . $row->address;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['name'] = $row->name;
            $nestedData['weight'] = $row->weight . ' Kg';
            $nestedData['age'] = Main::format_age($row->birthday);
            $nestedData['done_time'] = Main::format_datetime($row->done_time);
            $nestedData['address'] = $address;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-medic_record_print dropdown-item"
                           href="' . route('patientMedicRecordPrint', ['id_patient' => $id_patient]) . '"
                           target="_blank">
                            <i class="la la-user-secret"></i>
                            Cetak Rekam Medis
                        </a>
                        <a class="akses-payment_print dropdown-item"
                           href="' . route('paymentPrint', ['id_payment' => $id_control, 'type' => 'control']) . '"
                           target="_blank">
                            <i class="la la-user-secret"></i>
                            Cetak Payment
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="akses-done_back dropdown-item"
                           href="' . route('doneBackProcess', ['id_done' => $id_done, 'date_from' => $date_from, 'date_to' => $date_to, 'name' => $name]) . '">
                            <i class="la la-backward"></i>
                            Kembalikan ke Kontrol
                        </a>
                        <a class="akses-detail dropdown-item btn-modal-general"
                           href="#"
                           data-route="' . route('doneDetail', ['id_done' => $id_done]) . '"
                            >
                            <i class="la la-info"></i>
                            Detail
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function back_process(Request $request, $id_done)
    {
        $id_done = Main::decrypt($id_done);
        $id_control = mDone::where('id_done', $id_done)->value('id_control');
        $control = mControl::where('id_control', $id_control)->first();
        $name = $request->name;
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date('d-m-Y');

        DB::beginTransaction();
        try {
            mDone
                ::where([
                    'id_done' => $id_done
                ])
                ->update([
                    'status' => 'back',
                    'back_time' => date('Y-m-d H:i:s')
                ]);

            mControl
                ::where([
                    'id_control' => $id_control
                ])
                ->update([
                    'status' => 'process'
                ]);

            mAppointment::where('id_appointment', $control->id_appointment)->update(['status' => 'prepare']);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

        return redirect()->route('doneList', ['date_from' => $date_from, 'date_to' => $date_to, 'name' => $name]);
    }

    function detail($id_done)
    {
        $id_done = Main::decrypt($id_done);

        $row = mDone
            ::with([
                'patient',
                'patient.province',
                'patient.city',
                'patient.subdistrict',
                'control',
                'control.action',
                'control.action.consult',
                'control.action.consult.appointment',
                'control.action.appointment',
                'control.action.appointment',
                'control.control_list' => function ($query) {
                    $query->orderBy('control_number', 'ASC');
                }
            ])
            ->where('id_done', $id_done)
            ->first();

        $data = [
            'row' => $row
        ];

        return view('done/done/doneDetail', $data);
    }


    function export_excel(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $data_list = mDone
            ::with([
                'patient',
                'patient.city',
                'patient.province',
                'patient.subdistrict',
            ])
            ->leftJoin('patient', 'patient.id_patient', '=', 'done.id_patient')
            ->whereIn('done.status', ['done'])
            ->whereBetween('done.done_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->orderBy('name', 'ASC')
            ->get();

        $data = [
            'data_list' => $data_list,
            'date_from' => $date_from,
            'date_to' => $date_to
        ];

        return view('done/done/doneExcel', $data);
    }

}
