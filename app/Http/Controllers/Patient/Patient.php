<?php

namespace app\Http\Controllers\Patient;

use app\Models\mAction;
use app\Models\mAppointment;
use app\Models\mCity;
use app\Models\mMedicRecord;
use app\Models\mPatient;
use app\Models\mPayment;
use app\Models\mProvince;
use app\Models\mSubdistrict;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;

class Patient extends Controller
{
    private $breadcrumb;
    private $menu_active;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menu_active = $cons['patient'];
        $this->breadcrumb = [
            [
                'label' => $cons['patient'],
                'route' => route('patientList')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $name = $request->name;
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y");
        $datatable_column = [
            ["data" => "no"],
            ["data" => "medic_record_number"],
            ["data" => "name"],
            ["data" => "birth_place"],
            ["data" => "age"],
            ["data" => "appointment_time"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'name' => $name,
            'date_from' => $date_from,
            'date_to' => $date_to
        ));

        return view('patient/patient/patientList', $data);
    }

    function data_table(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $name = $request->name;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $total_data = mPatient
            ::whereBetween('patient.created_at', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_patient'; //$columns[$request->input('order.0.column')];
        $order_type = 'asc';//$request->input('order.0.dir');

        $data_list = mPatient
            ::with([
                'city',
                'province',
                'subdistrict',
                'doctor'
            ])
            ->whereBetween('patient.created_at', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->where('patient.name', 'LIKE', '%' . $name . '%')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_patient = Main::encrypt($row->id_patient);
            $appointment_time = mAppointment
                ::where('id_patient', $row->id_patient)
                ->orderBy('id_appointment', 'DESC')
                ->value('appointment_time');

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['medic_record_number'] = $row->medic_record_number;
            $nestedData['name'] = $row->name;
            $nestedData['birth_place'] = $row->birth_place;
            $nestedData['age'] = Main::format_age($row->birthday);
            $nestedData['appointment_time'] = Main::format_datetime($appointment_time);
            $nestedData['options'] = '';

            if ($row->medic_record) {
                $nestedData['options'] = '<div class="dropdown">
                                                <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                                                        type="button"
                                                        id="dropdownMenuButton" data-toggle="dropdown"
                                                        aria-haspopup="true"
                                                        aria-expanded="false">
                                                            Menu
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right"
                                                     aria-labelledby="dropdownMenuButton">
                                                    <a class="akses-medic_record_print dropdown-item"
                                                       href="' . route('patientMedicRecordPrint', ['id_patient' => $id_patient]) . '"
                                                       target="_blank">
                                                        <i class="la la-user-secret"></i>
                                                        Cetak Rekam Medis
                                                    </a>
                                                    <a class="akses-medic_record_print dropdown-item"
                                                       href="' . route('patientMedicRecordEdit', ['id_patient' => $id_patient]) . '">
                                                        <i class="la la-user-secret"></i>
                                                        Edit Rekam Medis
                                                    </a>
                                                    <div class="dropdown-divider hidden"></div>
                                                    <a class="akses-detail dropdown-item hidden" href="#" data-toggle="modal"
                                                       data-target="#modal-patient-detail">
                                                        <i class="la la-info"></i>
                                                        Detail
                                                    </a>
                                                    <a class="akses-edit dropdown-item hidden" href="#" data-toggle="modal"
                                                       data-target="#modal-patient-edit">
                                                        <i class="la la-pencil"></i>
                                                        Edit
                                                    </a>
                                                    <div class="dropdown-divider hidden"></div>
                                                    <a class="akses-delete dropdown-item hidden" href="#">
                                                        <i class="la la-remove"></i>
                                                        Hapus
                                                    </a>
                                                </div>
                                            </div>';
            }


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function medic_record_edit($id_patient)
    {
        $breadcrumb = array_merge($this->breadcrumb, array(array(
            'label' => 'Edit Rekam Medis',
            'route' => ''
        )));

        $data = Main::data($breadcrumb, $this->menu_active);
        $id_patient = Main::decrypt($id_patient);
        $patient = mPatient
            ::with([
                'medic_record'
            ])
            ->where('id_patient', $id_patient)
            ->first();
        $province = mProvince::orderBy('province_name', 'ASC')->get();
        $city = mCity::where('id_province', $patient->id_province)->orderBy('city_name', 'ASC')->get();
        $subdistrict = mSubdistrict::where('id_city', $patient->id_city)->orderBy('subdistrict_name', 'ASC')->get();

        $data = array_merge($data, [
            'patient' => $patient,
            'province' => $province,
            'city' => $city,
            'subdistrict' => $subdistrict
        ]);

        return view('patient/patient/patientMedicRecordEdit', $data);
    }

    function medic_record_update(Request $request, $id_patient)
    {
        $request->validate([
            'name' => 'required',
            'birthday' => 'required',
            'id_province' => 'required',
            'id_city' => 'required',
            'id_subdistrict' => 'required',
            'address' => 'required',
            'weight' => 'required',
            'phone_1' => 'required|regex:/(62)[0-9]{9}/',

            'disease_now' => 'required',
            'disease_before' => 'required',
            'medical_history' => 'required',
            'alergy_history' => 'required',
            'sign_tension' => 'required',
            'sign_temp' => 'required',
            'sign_pulse' => 'required',
            'sign_rr' => 'required',
            'general_condition' => 'required',
            'genital_check' => 'required',
            'assessment' => 'required',
            'planning' => 'required',

        ]);

        $id_patient = Main::decrypt($id_patient);

        $name = $request->name;
        $birthday = $request->birthday;
        $id_province = $request->id_province;
        $id_city = $request->id_city;
        $id_subdistrict = $request->id_subdistrict;
        $address = $request->address;
        $weight = $request->weight;
        $phone_1 = $request->phone_1;
        $phone_2 = $request->phone_2;

        $disease_now = $request->disease_now;
        $disease_before = $request->disease_before;
        $medical_history = $request->medical_history;
        $alergy_history = $request->alergy_history;
        $sign_tension = $request->sign_tension;
        $sign_temp = $request->sign_temp;
        $sign_pulse = $request->sign_pulse;
        $sign_rr = $request->sign_rr;
        $general_condition = $request->general_condition;
        $genital_check = $request->genital_check;
        $assessment = $request->assessment;
        $planning = $request->planning;


        $data_patient = [
            'id_province' => $id_province,
            'id_city' => $id_city,
            'id_subdistrict' => $id_subdistrict,
            'name' => $name,
            'birthday' => Main::format_date_db($birthday),
            'weight' => $weight,
            'address' => $address,
            'phone_1' => $phone_1,
            'phone_2' => $phone_2,
        ];

        DB::beginTransaction();
        try {
            mPatient::where('id_patient', $id_patient)->update($data_patient);

            $data_medic = [
                'disease_now' => $disease_now,
                'disease_before' => $disease_before,
                'medical_history' => $medical_history,
                'alergy_history' => $alergy_history,
                'sign_tension' => $sign_tension,
                'sign_temp' => $sign_temp,
                'sign_pulse' => $sign_pulse,
                'sign_rr' => $sign_rr,
                'general_condition' => $general_condition,
                'genital_check' => $genital_check,
                'assessment' => $assessment,
                'planning' => $planning
            ];
            mMedicRecord::where('id_patient', $id_patient)->update($data_medic);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }


    }

    function medic_record_print($id_patient)
    {
        $id_patient = Main::decrypt($id_patient);

        $patient = mPatient
            ::with([
                'medic_record',
                'province',
                'city',
                'subdistrict'
            ])
            ->where('id_patient', $id_patient)
            ->first();

        $data = [
            'patient' => $patient
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('patient/patient/patientMedicRecordPrint', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Payment Print');
    }

    function export_excel(Request $request)
    {
        $date_from = $request->date_from;
        $date_to = $request->date_to;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $data_list = mPatient
            ::with([
                'city',
                'province',
                'subdistrict',
            ])
            ->whereBetween('patient.created_at', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->orderBy('name', 'ASC')
            ->get();

        $data = [
            'data_list' => $data_list,
            'date_from' => $date_from,
            'date_to' => $date_to
        ];

        return view('patient/patient/patientExcel', $data);
    }

}
