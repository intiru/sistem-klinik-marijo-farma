<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mAction;
use app\Models\mActionMethod;
use app\Models\mDistrict;
use app\Models\mSubdistrict;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;

class Subdistrict extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_6'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mSubdistrict
            ::orderBy('sdt_name', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('masterData/subdistrict/subdistrictList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'sdt_name' => 'required',
        ]);

        $data = $request->except('_token');
        mSubdistrict::create($data);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mSubdistrict::where('id_subdistrict', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('masterData/subdistrict/subdistrictEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mSubdistrict::where('id_subdistrict', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'sdt_name' => 'required',
        ]);
        $data = $request->except("_token");
        mSubdistrict::where(['id_subdistrict' => $id])->update($data);
    }
}
