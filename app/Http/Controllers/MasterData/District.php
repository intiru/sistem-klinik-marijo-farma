<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mAction;
use app\Models\mActionMethod;
use app\Models\mDistrict;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;

class District extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_5'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mDistrict
            ::orderBy('dst_name', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('masterData/district/districtList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'dst_name' => 'required',
        ]);

        $data = $request->except('_token');
        mDistrict::create($data);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mDistrict::where('id_district', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('masterData/district/districtEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mDistrict::where('id_district', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'dst_name' => 'required',
        ]);
        $data = $request->except("_token");
        mDistrict::where(['id_district' => $id])->update($data);
    }
}
