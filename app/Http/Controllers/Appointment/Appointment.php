<?php

namespace app\Http\Controllers\Appointment;

use app\Mail\appointmentNotification;
use app\Mail\reminderSchedule;
use app\Models\mAction;
use app\Models\mAppointment;
use app\Models\mCity;
use app\Models\mConsult;
use app\Models\mControl;
use app\Models\mDistrict;
use app\Models\mKaryawan;
use app\Models\mPatient;
use app\Models\mProvince;
use app\Models\mReminderHistory;
use app\Models\mSubdistrict;
use app\Models\mUserRole;
use app\Rules\MedicRecordCheck;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class Appointment extends Controller
{
    private $breadcrumb;
    private $view = [
        'need_type' => ["search_field" => "appointment.need_type"],
        'name' => ["search_field" => "patient.name"],
        'weight' => ["search_field" => "patient.name"],
        'birthday' => ["search_field" => "patient.birthday"],
        'appointment_time' => ["search_field" => "appointment.appointment_time"],
        'phone_1' => ["search_field" => "patient.phone_1"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['appointment'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $name = $request->name;
        $medic_record_number = $request->medic_record_number;
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y");
        //$request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));
        $province = mProvince::orderBy('province_name', 'ASC')->get();
        $dokter = mKaryawan::where('posisi_karyawan', 'Dokter')->orderBy('nama_karyawan', 'ASC')->get();
        $perawat = mKaryawan::where('posisi_karyawan', 'Perawat')->orderBy('nama_karyawan', 'ASC')->get();
        $district = mDistrict::orderBy('dst_name', 'ASC')->get();
        $subdistrict = mSubdistrict::orderBy('sdt_name', 'ASC')->get();
        $datatable_column = [
            ["data" => "no"],
//            ["data" => "need_type"],
            ["data" => "medic_record_number"],
            ["data" => "name"],
            ["data" => "birth_place"],
//            ["data" => "weight"],
            ["data" => "age"],
            ["data" => "appointment_time"],
//            ["data" => "location"],
//            ["data" => "reminder_status"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'province' => $province,
            'district' => $district,
            'subdistrict' => $subdistrict,
            'name' => $name,
            'medic_record_number' => $medic_record_number,
            'dokter' => $dokter,
            'perawat' => $perawat,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'datatable_column' => $datatable_column
        ]);


        return view('appointment/appointment/appointmentList', $data);
    }

    function data_table(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $name = $request->name;
        $medic_record_number = $request->medic_record_number;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $total_data = mAppointment
            ::leftJoin('patient', 'patient.id_patient', '=', 'appointment.id_patient')
            ->whereIn('appointment.status', ['prepare', 'send'])
            ->whereBetween('appointment.appointment_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"]);

        if ($name) {
            $total_data = $total_data
                ->where('patient.name', 'LIKE', '%' . $name . '%');
        }
        if ($medic_record_number) {
            $total_data = $total_data
                ->where('patient.medic_record_number', 'LIKE', '%' . $medic_record_number . '%');
        }
        $total_data = $total_data
            ->count();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_appointment'; //$columns[$request->input('order.0.column')];
        $order_type = 'asc';//$request->input('order.0.dir');

        $data_list = mAppointment
            ::select([
                'appointment.*',
                'patient.*',
                'appointment.status AS appointment_status'
            ])
            ->leftJoin('patient', 'patient.id_patient', '=', 'appointment.id_patient')
            ->whereIn('appointment.status', ['prepare', 'send'])
            ->whereBetween('appointment.appointment_time', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"]);

        if($name) {
            $data_list = $data_list->where('patient.name', 'LIKE', '%' . $name . '%');
        }
        if($medic_record_number) {
            $data_list = $data_list->where('patient.medic_record_number', 'LIKE', '%' . $medic_record_number . '%');
        }
        $data_list = $data_list
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $location = '';
            if ($row->need_type == 'consult') {
                $location = mConsult::select('consult_location')->where('id_consult', $row->id_consult)->value('consult_location');
            } else if ($row->need_type == 'action') {
                $location = mAction::select('action_location')->where('id_action', $row->id_action)->value('action_location');
            } else if ($row->need_type == 'control') {
                $location = mControl::select('control_location')->where('id_control', $row->id_control)->value('control_location');
            }

            $nestedData['no'] = $no;
            $nestedData['need_type'] = Main::need_type_span($row->need_type, $row->control_step);
            $nestedData['name'] = $row->name;
            $nestedData['medic_record_number'] = $row->patient->medic_record_number;
            $nestedData['birth_place'] = $row->patient->birth_place;
            $nestedData['weight'] = $row->weight . ' Kg';
            $nestedData['age'] = Main::format_age($row->birthday);
            $nestedData['appointment_time'] = Main::format_datetime($row->appointment_time);
            $nestedData['location'] = ucwords($location);
            $nestedData['reminder_status'] = $row->whatsapp_send == 'yes' ? Main::appointment_status($row->appointment_status) : '<span class="m-badge m-badge--default m-badge--wide">Tidak Direminder</span>';
            $nestedData['options'] = '
                
                    <div class="dropdown" data-id-appointment="' . $row->id_appointment . '">
                        <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                                type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            Menu
                        </button>

                        <div class="dropdown-menu dropdown-menu-right"
                             aria-labelledby="dropdownMenuButton">
                            <a class="akses-detail dropdown-item btn-modal-general m--hide"
                                href="#"
                               data-route="' . route('appointmentDetail', ['id_appointment' => Main::encrypt($row->id_appointment)]) . '">
                                <i class="la la-info"></i>
                                Detail
                            </a>
                            <a class="dropdown-item akses-cetak-antrian"
                                href="' . route('appointmentQueuePrint', ['id_appointment' => Main::encrypt($row->id_appointment)]) . '"
                                target="_blank">
                                <i class="la la-info"></i>
                                Cetak Antrian
                            </a>
                            <a class="akses-edit dropdown-item btn-modal-general"
                               href="#"
                               data-route="' . route('appointmentNew', ['id_appointment' => Main::encrypt($row->id_appointment)]) . '">
                                <i class="la la-plus"></i>
                                Buat Tindakan
                            </a>
                            <a class="akses-edit dropdown-item btn-modal-general"
                               href="#"
                               data-route="' . route('appointmentEdit', ['id_appointment' => Main::encrypt($row->id_appointment)]) . '">
                                <i class="la la-pencil"></i>
                                Edit
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="akses-delete dropdown-item btn-hapus"
                               data-route="' . route('appointmentDelete', ['id_appointment' => Main::encrypt($row->id_appointment)]) . '"
                               href="#">
                                <i class="la la-remove"></i>
                                Hapus
                            </a>
                        </div>
                    </div>
                ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function detail($id_appointment = '')
    {
        $id_appointment = Main::decrypt($id_appointment);
        $row = mAppointment
            ::with([
                'patient',
                'patient.province',
                'patient.city',
                'patient.subdistrict'
            ])
            ->where('id_appointment', $id_appointment)
            ->first();
        $data = [
            'row' => $row
        ];

        return view('appointment/appointment/appointmentDetail', $data);
    }

    function member_new()
    {
        $province = mProvince::orderBy('province_name', 'ASC')->get();
        $data = [
            'province' => $province
        ];
        return view('appointment/appointment/appointmentMemberNew', $data);

    }

    function member_new_insert(Request $request)
    {
        $request->validate([
//            'need_type' => 'required',
//            'location' => 'required',
            'medic_record_number' => [
                'required',
//                new MedicRecordCheck()
            ],
            'name' => 'required',
            'birthday' => 'required|date_format:d-m-Y',
//            'id_province' => 'required',
//            'id_city' => 'required',
//            'id_subdistrict' => 'required',
            'address' => 'required',
            'weight' => 'required',
            'appointment_date' => 'required|date_format:d-m-Y|after:' . date('Y-m-d', strtotime('yesterday')),
            'appointment_hours' => 'required',
//            'phone_1' => 'required|regex:/(62)[0-9]{9}/',
        ]);

        $need_type = $request->need_type;
        $control_step = $request->control_step;
        $location = $request->location;
        $name = $request->name;
        $name_responsible = $request->name_responsible;
        $name_responsible_relation = $request->name_responsible_relation;
        $birth_place = $request->birth_place;
        $birthday = $request->birthday;
        $id_province = $request->id_province;
        $id_city = $request->id_city;
        $id_subdistrict = $request->id_subdistrict;
        $id_karyawan_doctor = $request->id_karyawan_doctor;
        $id_karyawan_nurse = $request->id_karyawan_nurse;
        $temperature = $request->temperature;
        $complaint = $request->complaint;
        $district = $request->district;
        $subdistrict = $request->subdistrict;
        $address = $request->address;
        $weight = $request->weight;
        $poli = $request->poli;
        $medic_record_number = $request->medic_record_number;
        $appointment_date = $request->appointment_date;
        $appointment_hours = $request->appointment_hours;
        $appointment_time = Main::format_date_db($appointment_date) . ' ' . Main::format_time_db($appointment_hours);
        $phone_1 = $request->phone_1;
        $phone_2 = $request->phone_2;
        $id_user = Session::get('user')['id'];
        $via = $request->via;

        DB::beginTransaction();
        try {

            $data_patient = [
                'id_province' => $id_province,
                'id_city' => $id_city,
                'id_subdistrict' => $id_subdistrict,
                'id_user' => $id_user,
                'id_karyawan_doctor' => $id_karyawan_doctor,
                'id_karyawan_nurse' => $id_karyawan_nurse,
                'name' => $name,
                'name_responsible' => $name_responsible,
                'name_responsible_relation' => $name_responsible_relation,
                'birth_place' => $birth_place,
                'birthday' => Main::format_date_db($birthday),
                'temperature' => $temperature,
                'complaint' => $complaint,
                'district' => $district,
                'subdistrict' => $subdistrict,
                'medic_record_number' => $medic_record_number,
                'weight' => $weight,
                'address' => $address,
                'phone_1' => $phone_1,
                'phone_2' => $phone_2,
                'poli' => $poli,
                'status' => 'appointment',
            ];

            $id_patient = mPatient::create($data_patient)->id_patient;

            $data_appointment = [
                'id_patient' => $id_patient,
                'id_user' => $id_user,
                'need_type' => $need_type,
                'appointment_time' => $appointment_time,
                'via' => $via,
                'status' => 'prepare'
            ];

            if ($need_type == 'control') {
                $data_appointment['control_step'] = $control_step;
            }

            $id_appointment = mAppointment::create($data_appointment)->id_appointment;

            if ($need_type == 'consult') {
                $data_consult = [
                    'id_patient' => $id_patient,
                    'id_appointment' => $id_appointment,
                    'id_user' => $id_user,
                    'consult_location' => $location,
                    'consult_time' => Main::format_datetime_db($appointment_time),
                    'status' => 'process'
                ];
                $id_consult = mConsult::create($data_consult)->id_consult;

                mAppointment::where('id_appointment', $id_appointment)->update(['id_consult' => $id_consult]);


            } elseif ($need_type == 'action') {
                $data_action = [
                    'id_patient' => $id_patient,
                    'id_consult' => '',
                    'id_appointment' => $id_appointment,
                    'id_user' => $id_user,
                    'action_location' => $location,
                    'action_time' => Main::format_datetime_db($appointment_time),
                    'status' => 'process'
                ];

                $id_action = mAction::create($data_action)->id_action;

                mAppointment::where('id_appointment', $id_appointment)->update(['id_action' => $id_action]);
            } elseif ($need_type == 'control') {

                $data_control = [
                    'id_patient' => $id_patient,
                    'id_action' => '',
                    'id_appointment' => $id_appointment,
                    'id_user' => $id_user,
                    'control_location' => $location,
                    'control_time' => Main::format_datetime_db($appointment_time),
                    'status' => 'process',
                    'control_number' => $control_step
                ];

                $id_control = mControl::create($data_control)->id_control;

                mAppointment::where('id_appointment', $id_appointment)->update(['id_control' => $id_control]);
            }

            if ($via == 'user') {
                Mail::to("sunatuntuksemua@gmail.com")->send(new appointmentNotification($id_appointment));

                $data_reminder_history = [
                    'status' => 'success',
                    'title' => 'New Appointment Notification',
                    'message' => 'Check Inbox Email sunatuntuksemua@gmail.com',
                    'email' => 'sunatuntuksemua@gmail.com',
                    'id_patient' => $id_patient,
                    'type' => 'reminder_new_appointment',
                    'id_appointment' => $id_appointment
                ];

                mReminderHistory::create($data_reminder_history);
            }
            DB::commit();

        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

//        return [
//            'redirect' => route('appointmentQueuePrint', ['id_appointment' => Main::encrypt($id_appointment)])
//        ];

//        return Response::json(array(
//            'code'      =>  404,
//            'message'   =>  'test'
//        ), 404);


    }

    function queue_print($id_appointment)
    {
        $id_appointment = Main::decrypt($id_appointment);
        $appointment = mAppointment::with('patient')->where('id_appointment', $id_appointment)->first();
        $dokter = mKaryawan::where('id', $appointment->patient->id_karyawan_doctor)->first();
        $perawat = mKaryawan::where('id', $appointment->patient->id_karyawan_nurse)->first();
        $no_antrian = 0;
        $appointment_list = mAppointment
            ::whereDate('appointment_time', date('Y-m-d', strtotime($appointment->appointment_time)))
            ->orderBy('id_appointment')
            ->get();
        foreach ($appointment_list as $key => $row) {
            $no = $key + 1;
            if ($row->id_appointment == $id_appointment) {
                $no_antrian = $no;
            }
        }


        $data = [
            'appointment' => $appointment,
            'dokter' => $dokter,
            'perawat' => $perawat,
            'no_antrian' => $no_antrian
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('appointment.appointment.appointmentQueuePdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Payment Print');

    }

    function edit($id_appointment)
    {
        $id_appointment = Main::decrypt($id_appointment);

        $edit = mAppointment::with('patient')->where('id_appointment', $id_appointment)->first();
        $province = mProvince::orderBy('province_name', 'ASC')->get();
        $city = mCity::where('id_province', $edit->patient->id_province)->orderBy('city_name', 'ASC')->get();
//        $subdistrict = mSubdistrict::where('id_city', $edit->patient->id_city)->orderBy('subdistrict_name', 'ASC')->get();
        $dokter = mKaryawan::where('posisi_karyawan', 'Dokter')->orderBy('nama_karyawan', 'ASC')->get();
        $perawat = mKaryawan::where('posisi_karyawan', 'Perawat')->orderBy('nama_karyawan', 'ASC')->get();
        $district = mDistrict::orderBy('dst_name', 'ASC')->get();
        $subdistrict_2 = mSubdistrict::orderBy('sdt_name', 'ASC')->get();

        $location = '';
        if ($edit->need_type == 'consult') {
            $location = mConsult::where('id_consult', $edit->id_consult)->value('consult_location');
        } else if ($edit->need_type == 'action') {
            $location = mAction::where('id_action', $edit->id_action)->value('action_location');
        } else if ($edit->need_type == 'control') {
            $location = mControl::where('id_control', $edit->id_control)->value('control_location');
        }
        $data = [
            'edit' => $edit,
            'province' => $province,
            'city' => $city,
//            'subdistrict' => $subdistrict,
            'location' => $location,
            'dokter' => $dokter,
            'perawat' => $perawat,
            'district' => $district,
            'subdistrict_2' => $subdistrict_2
        ];

        return view('appointment/appointment/appointmentEdit', $data);
    }

    function appointment($id_appointment)
    {
        $id_appointment = Main::decrypt($id_appointment);

        $edit = mAppointment::with('patient')->where('id_appointment', $id_appointment)->first();
        $province = mProvince::orderBy('province_name', 'ASC')->get();
        $city = mCity::where('id_province', $edit->patient->id_province)->orderBy('city_name', 'ASC')->get();
//        $subdistrict = mSubdistrict::where('id_city', $edit->patient->id_city)->orderBy('subdistrict_name', 'ASC')->get();
        $dokter = mKaryawan::where('posisi_karyawan', 'Dokter')->orderBy('nama_karyawan', 'ASC')->get();
        $perawat = mKaryawan::where('posisi_karyawan', 'Perawat')->orderBy('nama_karyawan', 'ASC')->get();
        $district = mDistrict::orderBy('dst_name', 'ASC')->get();
        $subdistrict_2 = mSubdistrict::orderBy('sdt_name', 'ASC')->get();

        $location = '';
        if ($edit->need_type == 'consult') {
            $location = mConsult::where('id_consult', $edit->id_consult)->value('consult_location');
        } else if ($edit->need_type == 'action') {
            $location = mAction::where('id_action', $edit->id_action)->value('action_location');
        } else if ($edit->need_type == 'control') {
            $location = mControl::where('id_control', $edit->id_control)->value('control_location');
        }
        $data = [
            'edit' => $edit,
            'province' => $province,
            'city' => $city,
//            'subdistrict' => $subdistrict,
            'location' => $location,
            'dokter' => $dokter,
            'perawat' => $perawat,
            'district' => $district,
            'subdistrict_2' => $subdistrict_2
        ];

        return view('appointment/appointment/appointmentNew', $data);
    }

    function update(Request $request, $id_appointment)
    {

        $request->validate([
//            'need_type' => 'required',
            'name' => 'required',
//            'location' => 'required',
            'birthday' => 'required|date_format:d-m-Y',
//            'id_province' => 'required',
//            'id_city' => 'required',
//            'id_subdistrict' => 'required',
            'address' => 'required',
            'weight' => 'required',
            'appointment_date' => 'required',
            'appointment_hours' => 'required',
            'phone_1' => 'required|regex:/(62)[0-9]{9}/',
        ]);

        $id_appointment = Main::decrypt($id_appointment);

        $id_patient = $request->id_patient;
        $need_type_new = $request->need_type;
        $control_step = $request->control_step;
        $location = $request->location;
        $name = $request->name;
        $name_responsible = $request->name_responsible;
        $name_responsible_relation = $request->name_responsible_relation;
        $birth_place = $request->birth_place;
        $birthday = $request->birthday;
        $temperature = $request->temperature;
        $complaint = $request->complaint;
        $medic_record_number = $request->medic_record_number;
        $id_province = $request->id_province;
        $id_city = $request->id_city;
        $id_subdistrict = $request->id_subdistrict;
        $district = $request->district;
        $subdistrict = $request->subdistrict;
        $address = $request->address;
        $weight = $request->weight;
        $id_karyawan_doctor = $request->id_karyawan_doctor;
        $id_karyawan_nurse = $request->id_karyawan_nurse;
        $appointment_date = $request->appointment_date;
        $appointment_hours = $request->appointment_hours;
        $appointment_time = Main::format_date_db($appointment_date) . ' ' . Main::format_time_db($appointment_hours);
        $phone_1 = $request->phone_1;
        $phone_2 = $request->phone_2;
        $poli = $request->poli;
        $appointment = mAppointment::where('id_appointment', $id_appointment)->first();
        $need_type_old = $appointment->need_type;
        $id_user = Session::get('user')['id'];

        DB::beginTransaction();
        try {
            $data_patient = [
                'id_province' => $id_province,
                'id_city' => $id_city,
                'id_subdistrict' => $id_subdistrict,
                'id_user' => $id_user,
                'id_karyawan_doctor' => $id_karyawan_doctor,
                'id_karyawan_nurse' => $id_karyawan_nurse,
                'name' => $name,
                'name_responsible' => $name_responsible,
                'name_responsible_relation' => $name_responsible_relation,
                'birth_place' => $birth_place,
                'birthday' => Main::format_date_db($birthday),
                'temperature' => $temperature,
                'complaint' => $complaint,
                'district' => $district,
                'subdistrict' => $subdistrict,
                'medic_record_number' => $medic_record_number,
                'weight' => $weight,
                'address' => $address,
                'phone_1' => $phone_1,
                'phone_2' => $phone_2,
                'poli' => $poli
            ];
            mPatient::where('id_patient', $id_patient)->update($data_patient);

            $data_appointment = [
                'id_patient' => $id_patient,
                'need_type' => $need_type_new,
                'appointment_time' => Main::format_datetime_db($appointment_time),
            ];

            if ($need_type_new == 'control') {
                $data_appointment['control_step'] = $control_step;
            }

            mAppointment::where('id_appointment', $id_appointment)->update($data_appointment);

            if ($need_type_old == 'consult' && $need_type_new == 'consult') {

                $check_consult_exist = mConsult::where('id_appointment', $id_appointment)->count();
                if ($check_consult_exist == 0) {
                    $data_consult = [
                        'id_patient' => $id_patient,
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'consult_location' => $location,
                        'consult_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process'
                    ];
                    $id_consult = mConsult::create($data_consult)->id_consult;
                    mAppointment::where('id_appointment', $id_appointment)->update(['id_consult' => $id_consult]);
                } else {
                    $data_consult = [
                        'consult_time' => Main::format_datetime_db($appointment_time),
                        'consult_location' => $location,
                        'status' => 'process'
                    ];

                    mConsult::where('id_consult', $appointment->id_consult)->update($data_consult);
                }
            } elseif ($need_type_old == 'action' && $need_type_new == 'action') {

                $check_action_exist = mAction::where('id_appointment', $id_appointment)->count();
                if ($check_action_exist == 0) {
                    $data_action = [
                        'id_patient' => $id_patient,
                        'id_consult' => $appointment->id_consult,
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'action_location' => $location,
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process'
                    ];
                    $id_action = mAction::create($data_action)->id_action;
                    mAppointment::where('id_appointment', $id_appointment)->update(['id_action' => $id_action]);
                } else {

                    $data_action = [
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'action_location' => $location,
                        'status' => 'process'
                    ];

                    mAction::where('id_action', $appointment->id_action)->update($data_action);
                }

            } elseif ($need_type_old == 'control' && $need_type_new == 'control') {

                $check_control_exist = mControl::where('id_appointment', $id_appointment)->count();
                if ($check_control_exist == 0) {
                    $data_control = [
                        'id_appointment' => $id_appointment,
                        'id_patient' => $id_patient,
                        'id_action' => $appointment->id_action,
                        'id_user' => $id_user,
                        'control_location' => $location,
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                        'control_number' => $control_step,
                    ];
                    $id_control = mControl::create($data_control)->id_control;
                    mAppointment::where('id_appointment', $id_appointment)->update(['id_control' => $id_control]);
                } else {
                    $data_control = [
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'control_location' => $location,
                        'status' => 'process'
                    ];

                    mControl::where('id_control', $appointment->id_control)->update($data_control);
                }

            } elseif ($need_type_old == 'consult' && $need_type_new == 'action') {
                $id_consult = $appointment->id_consult;
                $check_appointment_action_exist = mAction::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_action_exist == 0) {
                    $data_action = [
                        'id_patient' => $id_patient,
                        'id_consult' => '',
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'action_location' => $location,
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                    ];

                    $id_action = mAction::create($data_action)->id_action;
                } else {
                    $id_action = mAction::where('id_appointment', $id_appointment)->value('id_action');
                    $data_action = [
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'action_location' => $location,
                        'status' => 'process'
                    ];
                    mAction::where('id_action', $id_action)->update($data_action);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_action' => $id_action]);

                mConsult
                    ::where('id_consult', $id_consult)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            } elseif ($need_type_old == 'consult' && $need_type_new == 'control') {
                $id_consult = $appointment->id_consult;
                $check_appointment_control_exist = mControl::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_control_exist == 0) {
                    $data_control = [
                        'id_appointment' => $id_appointment,
                        'id_patient' => $id_patient,
                        'id_action' => '',
                        'id_user' => $id_user,
                        'control_location' => $location,
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                        'control_number' => $control_step,
                    ];

                    $id_control = mControl::create($data_control)->id_control;
                } else {
                    $id_control = mControl::where('id_appointment', $id_appointment)->value('id_control');
                    $data_control = [
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'control_location' => $location,
                        'control_number' => $control_step,
                        'status' => 'process'
                    ];
                    mControl::where('id_control', $id_control)->update($data_control);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_control' => $id_control, 'control_step' => $control_step]);

                mConsult
                    ::where('id_consult', $id_consult)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            } elseif ($need_type_old == 'action' && $need_type_new == 'consult') {
                $id_action = $appointment->id_action;
                $check_appointment_consult_exist = mConsult::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_consult_exist == 0) {
                    $data_consult = [
                        'id_patient' => $id_patient,
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'consult_location' => $location,
                        'consult_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                    ];

                    $id_consult = mConsult::create($data_consult)->id_consult;
                } else {
                    $id_consult = mConsult::where('id_appointment', $id_appointment)->value('id_consult');
                    $data_consult = [
                        'consult_time' => Main::format_datetime_db($appointment_time),
                        'consult_location' => $location,
                        'status' => 'process'
                    ];
                    mConsult::where('id_consult', $id_consult)->update($data_consult);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_consult' => $id_consult]);

                mAction
                    ::where('id_action', $id_action)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            } elseif ($need_type_old == 'action' && $need_type_new == 'control') {
                $id_action = $appointment->id_action;
                $check_appointment_control_exist = mControl::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_control_exist == 0) {
                    $data_control = [
                        'id_patient' => $id_patient,
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'id_action' => '',
                        'control_location' => $location,
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                        'control_number' => $control_step,
                    ];

                    $id_control = mControl::create($data_control)->id_control;
                } else {
                    $id_control = mControl::where('id_appointment', $id_appointment)->value('id_control');
                    $data_control = [
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'control_location' => $location,
                        'control_number' => $control_step,
                        'status' => 'process'
                    ];
                    mControl::where('id_control', $id_control)->update($data_control);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_control' => $id_control, 'control_step' => $control_step]);

                mAction
                    ::where('id_action', $id_action)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            } elseif ($need_type_old == 'control' && $need_type_new == 'consult') {
                $id_control = $appointment->id_control;
                $check_appointment_consult_exist = mConsult::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_consult_exist == 0) {
                    $data_consult = [
                        'id_patient' => $id_patient,
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'control_location' => $location,
                        'control_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                    ];

                    $id_consult = mConsult::create($data_consult)->id_consult;
                } else {
                    $id_consult = mConsult::where('id_appointment', $id_appointment)->value('id_consult');
                    $data_consult = [
                        'consult_time' => Main::format_datetime_db($appointment_time),
                        'consult_location' => $location,
                        'status' => 'process'
                    ];
                    mConsult::where('id_consult', $id_consult)->update($data_consult);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_consult' => $id_consult]);

                mControl
                    ::where('id_control', $id_control)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            } elseif ($need_type_old == 'control' && $need_type_new == 'action') {
                $id_control = $appointment->id_control;
                $check_appointment_action_exist = mAction::where('id_appointment', $id_appointment)->count();

                if ($check_appointment_action_exist == 0) {
                    $data_action = [
                        'id_patient' => $id_patient,
                        'id_consult' => '',
                        'id_appointment' => $id_appointment,
                        'id_user' => $id_user,
                        'action_location' => $location,
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'status' => 'process',
                    ];

                    $id_action = mAction::create($data_action)->id_action;
                } else {
                    $id_action = mAction::where('id_appointment', $id_appointment)->value('id_action');
                    $data_action = [
                        'action_time' => Main::format_datetime_db($appointment_time),
                        'action_location' => $location,
                        'status' => 'process'
                    ];
                    mAction::where('id_action', $id_action)->update($data_action);
                }
                mAppointment
                    ::where('id_appointment', $id_appointment)
                    ->update(['id_action' => $id_action]);

                mControl
                    ::where('id_control', $id_control)
                    ->update([
                        'status' => 'cancel',
                        'cancel_time' => date('Y-m-d H:i:s')
                    ]);
            }

            DB::commit();

        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function delete($id_appointment)
    {

        DB::beginTransaction();
        try {
            $id_appointment = Main::decrypt($id_appointment);
            $appointment = mAppointment::where('id_appointment', $id_appointment)->first();
            mAppointment::where('id_appointment', $id_appointment)->delete();
            mPatient::where('id_patient', $appointment->id_patient)->delete();

            if ($appointment->need_type == 'control') {
                mControl::where('id_control', $appointment->id_control)->delete();
            }

            if ($appointment->need_type == 'action') {
                mAction::where('id_action', $appointment->id_action)->delete();
            }

            if ($appointment->need_type == 'consult') {
                mConsult::where('id_consult', $appointment->id_consult)->delete();
            }

            DB::commit();

        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }


    }

}
