<?php

namespace app\Http\Controllers\ReminderHistory;

use app\Models\mPatient;
use app\Models\mReminderHistory;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;

class ReminderHistory extends Controller
{
    private $breadcrumb;
    private $view = [
        'nama_karyawan' => ["search_field" => "tb_karyawan.nama_karyawan"],
        'posisi_karyawan' => ["search_field" => "tb_karyawan.posisi_karyawan"],
        'telp_karyawan' => ["search_field" => "tb_karyawan.telp_karyawan"],
        'alamat_karyawan' => ["search_field" => "tb_karyawan.alamat_karyawan"],
        'email_karyawan' => ["search_field" => "tb_karyawan.email_karyawan"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['reminder_history'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_from = $request->date_from ? $request->date_from : date('01-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $datatable_column = [
            ["data" => "no"],
            ["data" => "title"],
            ["data" => "patient_name"],
            ["data" => "message"],
            ["data" => "created_at"],
            ["data" => "options"],
        ];

        $reminder_history = mReminderHistory
            ::with([
                'appointment:id_appointment,id_patient',
                'appointment.patient:id_patient,name',
                'patient:id_patient,name'
            ])
            ->whereBetween('created_at', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->orderBy('id_reminder_history', 'DESC')
            ->get();

        $data = array_merge($data, [
            'reminder_history' => $reminder_history,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'datatable_column' => $datatable_column
        ]);


        return view('reminderHistory/reminderHistory/reminderHistoryList', $data);
    }

    function data_table(Request $request)
    {

        $date_from = $request->date_from;
        $date_to = $request->date_to;

        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $total_data = mReminderHistory
            ::whereBetween('created_at', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_reminder_history'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mReminderHistory
            ::with([
                'appointment:id_appointment,id_patient',
                'appointment.patient:id_patient,name',
                'patient:id_patient,name',
                'action:id_action,id_patient',
                'action.patient:id_patient,name',
            ])
            ->whereBetween('created_at', [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_reminder_history = Main::encrypt($row->id_reminder_history);

            if ($row['appointment']['patient']['name']) {
                $name = $row['appointment']['patient']['name'];
            } elseif ($row['patient']['name']) {
                $name = $row['patient']['name'];
            } elseif ($row['action']['patient']['name']) {
                $name = $row['action']['patient']['name'];
            } else {
                $name = 'Admin';
            }

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['title'] = $row->title;
            $nestedData['patient_name'] = $name;
            $nestedData['message'] = substr($row->message, 0, 250) . ' ...';
            $nestedData['created_at'] = Main::format_datetime($row->created_at);
            $nestedData['options'] = '
                <button class="akses-detail btn btn-success btn-modal-general" data-route="' . route('reminderHistoryDetail', ['id_reminder_history' => $id_reminder_history]) . '">Detail</button>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function detail_modal($id_reminder_history)
    {
        $id_reminder_history = Main::decrypt($id_reminder_history);
        $reminder_history = mReminderHistory
            ::with([
                'appointment',
                'appointment.patient',
                'patient:id_patient,name'
            ])
            ->where('id_reminder_history', $id_reminder_history)
            ->first();

        $data = [
            'reminder_history' => $reminder_history
        ];

        return view('reminderHistory.reminderHistory.reminderHistoryDetailModal', $data);

    }

}
