<?php

namespace app\Rules;

use app\Models\mPatient;
use Illuminate\Contracts\Validation\Rule;
use app\Models\mUser;

class MedicRecordCheck implements Rule
{
    private $value = '';

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->value = $value;
        $count = mPatient::where('medic_record_number', $value)->count();
        if($count > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Nomer Rekam Medis \"<strong>".$this->value."</strong>\" Tidak Tersedia";
    }
}
